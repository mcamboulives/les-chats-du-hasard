<?php
/*
// "Easy Flexi Module" Module by CCC for Joomla! and Flexicontent 1.5.x
// Copyright (c) 2006 - 2010 ccomca.com All rights reserved.
// Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
// More info at http://www.ccomca.com/communaute/item/314-easy-flexi-module.html
// Designed and developed by the CCC team
// *** Last update: 29/04/2010 ***
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');


class modEasyFlexiModuleHelper
{
	function getList(&$params)
	{
		global $mainframe;

		$db			=& JFactory::getDBO();
		$user		=& JFactory::getUser();
		$ordering   = (int)$params->get('itemsOrdering', 0);
		$count		= intval($params->get('count', 5));
		$method		= (int)$params->get('method', '1');
		$scope		= $params->get('categories');
		$scope		= is_array($scope) ? implode(',', $scope) : $scope;
		
		$aid		= $user->get('aid', 0);

		$contentConfig = &JComponentHelper::getParams( 'com_content' );
		$access		= !$contentConfig->get('show_noauth');

		$nullDate	= $db->getNullDate();
		$date =& JFactory::getDate();
		$now  = $date->toMySQL();

		
		if ($method == 2) { // include method
			$catCondition = ' AND cc.id NOT IN (' . $scope . ')';		
		} else if ($method == 3) { // exclude method
			$catCondition = ' AND cc.id IN (' . $scope . ')';		
		}

		
		// ordering
		switch ($ordering) {
			case 0 :
				$orderby = 'a.created DESC';
				break;
			case 1 :
				$orderby = 'a.hits DESC';
				break;
			case 2 :
				$orderby = 'rand()';
				break;
			case 3 :
				$orderby = 'a.modified DESC';
				break;
			case 4 :
				$orderby = 'rel.ordering ASC';
				break;
			case 5 :
				$orderby = 'rel.ordering DESC';
				break;
			case 6 :
				$orderby = 'a.title ASC';
				break;
			case 7 :
				$orderby = 'a.title DESC';
				break;
				
			default :
				$orderby = 'a.created DESC';
				break;
		}

		//Content Items only
		$query = 'SELECT a.*,' .
			' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'.
			' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug'.
			' FROM #__content AS a' .
			' LEFT JOIN #__flexicontent_cats_item_relations AS rel ON rel.itemid = a.id' .
			' LEFT JOIN #__categories AS cc ON cc.id = rel.catid' .
			' WHERE ( a.state = 1 AND cc.id > 0 )' .
			' AND ( a.publish_up = '.$db->Quote($nullDate).' OR a.publish_up <= '.$db->Quote($now).' )' .
			' AND ( a.publish_down = '.$db->Quote($nullDate).' OR a.publish_down >= '.$db->Quote($now).' )'.
			($access ? ' AND a.access <= ' .(int) $aid. ' AND cc.access <= ' .(int) $aid : '').
			($catCondition ? $catCondition : '').
			' AND cc.published = 1' .
			' GROUP BY a.id' .
			' ORDER BY '.$orderby;
		$db->setQuery($query, 0, $count);
		$rows = $db->loadObjectList();
		$rows = FlexicontentFields::getFields ($rows, 'module');
		if ($params->get('image') && $params->get('layout_type') != 'simple') $dirimage = modEasyFlexiModuleHelper::getDirImage($params->get('image'));
		$i		= 0;
		$lists	= array();
		foreach ( $rows as $row )
		{
			if($row->access <= $aid)
			{
				//$lists[$i]->link = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug, $row->sectionid));
				$lists[$i]->link = JRoute::_(FlexicontentHelperRoute::getItemRoute($row->slug, $row->catslug));
			} else {
				$lists[$i]->link = JRoute::_('index.php?option=com_user&view=login');
			}
			$lists[$i]->text = htmlspecialchars( $row->title );
			
			if ($params->get('layout_type') != 'simple')
			{
				if ($params->get('use_image') == 1)
				{
					if ($params->get('image'))
					{
						//echo">";
						//$value 	= unserialize($row->fieldvalues[15][0]);
						//$image = $value['originalname'];
						//print_r($image);
						//print_r($row->fieldvalues[15][0]);
						//echo"<<br>";
						//if (isset($row->fields[$params->get('image')]->value[0]))
						if (isset($row->fieldvalues[15][0]))
						{
						// Fatal error: Call to a member function get() on a non-object
						//$dir =  $row->fields[$params->get('image')]->parameters->get('dir');
						$value 	= unserialize($row->fieldvalues[15][0]);
						$image = $value['originalname'];
						$src	= $dirimage.($params->get('image_size') ? '/'.$params->get('image_size').'_' : '/s_').$image;
						}
						else
						{
							$src = "";
						}
					}
					else
					{
						$src = flexicontent_html::extractimagesrc($row);	
					}
						
					$w		= '&w=' . $params->get('image_width', 180);
   					$h		= '&h=' . $params->get('image_height', 180);
    				$aoe	= '&aoe=1';
    				$q		= '&q=95';
    				$zc		= $params->get('image_method') ? '&zc=' . $params->get('image_method') : '';
    				$conf	= $w . $h . $aoe . $q . $zc;
    				
    				if (!$params->get('image_size')) :
    					 $thumb = JURI::base().'components/com_flexicontent/librairies/phpthumb/phpThumb.php?src='.JURI::base(true).'/'.$src.$conf;
    				else :
    					 $thumb = $src;
    				endif;
				}
			$lists[$i]->src = $src;
			$lists[$i]->thumb = $thumb;
			
			//date
			if ($params->get('display_date', 1) == 0) {
				
				if ($params->get('date', 1) == 1) {
					$lists[$i]->date = JHTML::_('date', $row->created, JText::_($params->get('format_date', 'DATE_FORMAT_LC3')));
				}
				else
				{
					$lists[$i]->date = JHTML::_('date', $row->modified, JText::_($params->get('format_date', 'DATE_FORMAT_LC3')));
				}
				
			}
			
			//description
			if ($params->get('strip_html_tags', 1) == 0) :
				$lists[$i]->description = flexicontent_html::striptagsandcut( $row->introtext, $params->get('introtext_max_length', 300) );
				else :
				$lists[$i]->description = $row->introtext;
				endif;
			}
			
			//custom field
			if ($params->get('layout_type') == 'custom')
			{
				$f = 1;
				while ($params->get( 'easyfleximodule_extendedfield_name_'.$f ) != "") 
					{
					$name = $params->get( 'easyfleximodule_extendedfield_name_'.$f );
					$lists[$i]->$name = $row->fields[$params->get( 'easyfleximodule_extendedfield_name_'.$f )]->display;;
					
					$f++;
					}

			}
			
			
			$i++;
		}

		return $lists;
	}
	
	function getDirImage ($field)
	{
		
		$db =& JFactory::getDBO();
		$query = 'SELECT attribs'
		. ' FROM #__flexicontent_fields'
		. ' WHERE published = 1'
		. ' AND name = ' . $db->Quote($field)
		. ' AND field_type = ' . $db->Quote('image')
		. ' ORDER BY label ASC, id ASC'
		;
		
		$db->setQuery($query);
		$items = $db->loadObjectList();
		
		foreach ($items as $item) {
		$paramfield = new JParameter( $item->attribs );
		}
		
		return $paramfield->get('dir');
		
	}
	
	
}