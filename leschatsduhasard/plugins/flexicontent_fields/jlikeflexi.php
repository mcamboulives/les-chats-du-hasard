<?php
 /**
 * @package Joomla
 * @subpackage FLEXIcontent
 * @subpackage plugin.jlikeflexi
 * @copyright (C) 2010 - Matthieu BARBE - www.ccomca.com
 * @license GNU/GPL v2
 * 
 *
 * jLikeFlexi uses :
 * Flexicontent => flexicontent.org
 * Logo by Custom Icon Design => http://www.customicondesign.com/
 * Like function by Facebook
 *
 * FLEXIcontent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */ 
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.event.plugin');

class plgFlexicontent_fieldsjLikeFlexi extends JPlugin
{
	function plgFlexicontent_fieldsjLikeFlexi( &$subject, $params )
	{
		parent::__construct( $subject, $params );
        JPlugin::loadLanguage('plg_flexicontent_fields_jlikeflexi', JPATH_ADMINISTRATOR);
	}

  // this shows up only in Article Edit at the BackEnd
	function onDisplayField(&$field, $item)
	{
		// execute the code only if the field type match the plugin type
		if($field->field_type != 'jlikeflexi') return;
 		// some parameter 
		// initialise property
		if($item->version < 2) {
			$field->value = array();
			$field->value[0] = 0;
		} elseif (!$field->value) {
			$field->value = array();
			$field->value[0] = '';
		}
		
		
		if ($field->value[0] == 1) $checked = 'checked="checked"'; else $checked = '';
     
        $field->html .= '<label><input type="checkbox" name="'.$field->name.'[activate]" value="1" '.$checked.'/> '.JText::_('FLEXI_FIELD_JLIKEFLEXI_DESACTIVATE').'</label><br />';
		 $field->html .= '<label><input type="text" name="'.$field->name.'[customurl]" value="" size="30"/> '.JText::_('FLEXI_FIELD_JLIKEFLEXI_CUSTOM_URL').'</label>';
         
	}

	function onBeforeSaveField( $field, &$post, &$file )
	{
		// execute the code only if the field type match the plugin type
		if($field->field_type != 'jlikeflexi') return;
		if(!$post) return;
				
		// debug
		//print_r ($post);    

		  if (isset($post["activate"])) {
		  $field->value['activate'] = 1;
		  } else {
		  $field->value['activate'] = 0;
		  }
	
  }

  // this is for the frontend display
	function onDisplayFieldValue(&$field, $item, $values=null, $prop='display')
	{
		// execute the code only if the field type match the plugin type
		if($field->field_type != 'jlikeflexi') return;
      
	  	global $mainframe, $xfbml;
		
	  
    	if ($field->value[0] != 1 ) {
		
			if ($field->value[1] == "")
			{
				//Thank you Edgar for your bug report => replace $item->catslug by $item->categoryslug
				$url = JURI::root().''.JRoute::_(FlexicontentHelperRoute::getItemRoute($item->slug, $item->categoryslug));
			}
			else
			{
				$url =	$field->value[1];
			}
    	if ($field->parameters->get( 'methods', 'iframe' ) == "iframe")
			{
//				$like = '<div class="jlikeflexi">
				$like = '
				<iframe src="http://www.facebook.com/plugins/like.php?href='.urlencode($url).'
				&amp;layout='.$field->parameters->get( 'layout', 'standard' ).'
				&amp;show_faces='. ($field->parameters->get('show_faces') == 'yes' ? 'true' : 'false').'
				&amp;width='.$field->parameters->get('width').'
				&amp;action='.$field->parameters->get('verb').'
				&amp;font='.urlencode($field->parameters->get('font')).'
				&amp;locale='.$field->parameters->get('lang', 'en_US').'
				&amp;colorscheme='.$field->parameters->get('color_scheme').'"
				scrolling="no"
				frameborder="0"
				allowTransparency="true"
				style="border:none;
				overflow:hidden;
				width:'.$field->parameters->get('width').'px;
				height:'.$field->parameters->get('height').'px">
				</iframe>
				';
//				</div>';
			}
			else
			{
				if (!$xfbml)
					{
					$like ="<div id=\"fb-root\"></div><script>window.fbAsyncInit = function() {
					FB.init({appId: '".$field->parameters->get('appid')."', status: true, cookie: true,
					xfbml: true});
					};
					(function() {
					var e = document.createElement('script'); e.async = true;
					e.src = document.location.protocol +
					'//connect.facebook.net/".$field->parameters->get('lang', 'en_US')."/all.js';
					document.getElementById('fb-root').appendChild(e);
					}());</script>
					";
					$xfbml = 1;
					}
				
				$like .= "<fb:like action='".$field->parameters->get('verb')."' colorscheme='".$field->parameters->get('color_scheme')."' layout='".$field->parameters->get( 'layout', 'standard' )."' show_faces='". ($field->parameters->get('show_faces') == 'yes' ? 'true' : 'false')."' width='".$field->parameters->get('width')."' href='".urlencode($url)."' font='".urlencode($field->parameters->get('font'))."' />";
			}
    
		// initialise property
		$field->display = $like;
		}
	}

}