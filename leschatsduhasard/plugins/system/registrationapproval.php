<?php
# Multilingual Registration Approval system plugin
# based on jspt_approve by Shyam Verma - http://www.joomlaxi.com
# authorurl http://joomla.fr
# copyright Shyam Verma , infograf768, Christophe Demko
# license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

defined('_JEXEC') or die('Restricted access to this plugin');
jimport( 'joomla.plugin.plugin' );

class  plgSystemRegistrationApproval extends JPlugin {
	var $_isAdmin=null;
	function plgSystemRegistrationApproval(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->_isAdmin=false;
		$lang =& JFactory::getLanguage();
		$lang->load('plg_system_registrationapproval', JPATH_ADMINISTRATOR);
	}

	function onAfterRoute ()
	{
		$mainframe =& JFactory::getApplication();
	
		$this->_isAdmin=false;
	   
		$option = JRequest::getCmd('option');
		$task = JRequest::getCmd('task');	
	
		if( !($option=='com_user' && $task=='activate'))
			return;

		jimport( 'joomla.filesystem.folder' );
	
		// Initialize some variables
		$db			=& JFactory::getDBO();
		$user 		=& JFactory::getUser();
	
		$usersConfig = &JComponentHelper::getParams( 'com_users' );
		$userActivation			= $usersConfig->get('useractivation');
		$allowUserRegistration	= $usersConfig->get('allowUserRegistration');
		$activation = JRequest::getVar('activation', '', '', 'alnum' );
		$activation = $db->getEscaped( $activation );
		
		if (empty( $activation ) || $user->get('id') || $allowUserRegistration == '0' || $userActivation == '0' ) 
			return;
		
		// Get the id of the user we want to activate
		$id = $this->_getUserID($activation);	
	
		$user =& JUser::getInstance((int)$id);
		$this->_isAdmin= $user->getParam('approve_admin','0') ? true : false;

		// Is it a valid user to activate?
		if (!$id)
			$pID=0;
	
		// Activate this user
		$message = JText::_('REGISAPPROVAL_ERROR');
		jimport('joomla.user.helper');
		if (!JUserHelper::activateUser($activation))
			$mainframe->redirect( 'index.php',$message);	
		
		// For admin : append a message  that activation is done
		// For user, block the user and notify to admin
		if($this->_isAdmin == true)
		{
			$message = JText::_('REGISAPPROVAL_ACCOUNT_ACTIVATED');		
			// send email to the user that account is active now and he can login.
			$this->_send_email($id,0);
		}
		else
		{
			$message =  JText::_('REGISAPPROVAL_EMAIL_VERIFIED');
			
			// also block the user
			$activation=JUtility::getHash( JUserHelper::genRandomPassword());
			$user =& JUser::getInstance((int)$id);
			$user->set('activation',$activation);
			$user->set('block', '1');
			$user->setParam('approve_admin','1');
		
			// save new activation key by which admin can enable user
			if (!$user->save())
			{
				JError::raiseWarning('', JText::_( $user->getError()));
				// redirect to index.php
				$mainframe->redirect('index.php','Some Error Occured in System');
				exit();
			}
				
			// send an email to admin asking to unblock the account
			$this->_send_email($id, $activation);
		}
	
		// redirect user/admin to frontpage with appropriate message
		$mainframe->redirect( 'index.php',$message);
	}

	function _getUserID($activation, $block=1)
	{
		$db			=& JFactory::getDBO();
		$query = 'SELECT id'
			. ' FROM #__users'
			. ' WHERE activation = '.$db->Quote($activation)
			. ' AND block = '.$db->Quote('1')
			. ' AND lastvisitDate = '.$db->Quote('0000-00-00 00:00:00');
			;
		$db->setQuery( $query );
		$id = $db->loadResult();
	
		return $id;
	}

	function _send_email($id, $activation)
	{
		$mainframe =& JFactory::getApplication();

		$db		=& JFactory::getDBO();
		$user 	=& JUser::getInstance((int)$id);
		$name 		= $user->name;
		$email 		= $user->email;
		$username 	= $user->username;

		$usersConfig 	= &JComponentHelper::getParams( 'com_users' );
		$sitename 		= $mainframe->getCfg( 'sitename' );
		$mailfrom 		= $mainframe->getCfg( 'mailfrom' );
		$fromname 		= $mainframe->getCfg( 'fromname' );
		$siteURL		= JURI::base();

		// send approval email
		if($this->_isAdmin==true)
		{	
			$sub =  JText::_('REGISAPPROVAL_ACCOUNT_APPROVED_SUBJECT');
			$subject 	= sprintf($sub,$siteURL,$user);
			$subject = html_entity_decode($subject, ENT_QUOTES, 'UTF-8');
		
			$msg	= JText::_('REGISAPPROVAL_ACCOUNT_APPROVED_EMAIL');
			$message = sprintf($msg,$name,$sitename,$name,$email,$username,$user);

			$message = html_entity_decode($message, ENT_QUOTES, 'UTF-8');
			JUtility::sendMail($mailfrom, $fromname, $email, $subject, $message);
		
			return;
		}
	
		// send request to approve to admin
		$actLink	= JRoute::_($siteURL."index.php?option=com_user&task=activate&activation="
									.$activation);
		$sub		= JText::_('REGISAPPROVAL_APPROVAL_REQUIRED_SUBJECT');
		$subject 	= sprintf($sub,$name,$sitename);
		$subject 	= html_entity_decode($subject, ENT_QUOTES, 'UTF-8');
	
		$msg	=	JText::_('REGISAPPROVAL_APPROVAL_REQUIRED_EMAIL');
		$message = sprintf($msg,$siteURL,$name,$email,$username,$actLink);
		$message = html_entity_decode($message, ENT_QUOTES, 'UTF-8');

		//get all administrators. If only superadmin approval is desired, take off the 24 below
				
		$gid = Array(25, 24);
		$query = 'SELECT name, email, sendEmail' .
                               ' FROM #__users' .
                               ' WHERE gid IN ('. implode(',', $gid) .')' .
                               ' AND sendEmail=1';
			
		$db->setQuery( $query );
		$rows = $db->loadObjectList();

		// Send mail to all  superadministrators id
		foreach( $rows as $row )
				JUtility::sendMail($mailfrom, $fromname, $row->email, $subject, $message);
	}
}
