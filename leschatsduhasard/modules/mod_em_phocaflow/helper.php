<?php
/**
* @version		$Id: helper.php 10214 2008-04-19 08:59:04Z eddieajau $
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

/** ensure this file is being included by a parent file */
defined('_JEXEC') or die('Direct Access to this location is not allowed.');

if (file_exists('components'.DS.'com_phocagallery'.DS.'helpers'.DS.'phocagallery.php')) {
	require_once( 'components'.DS.'com_phocagallery'.DS.'helpers'.DS.'phocagallery.php' );
}
else {
	if (!JComponentHelper::isEnabled('com_phocagallery', true)) {
	   return JError::raiseError(JText::_('Phoca Gallery Error'), JText::_('Phoca Gallery is not installed on your system'));
	}
	if (! class_exists('PhocaGalleryLoader')) {
	    require_once( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_phocagallery'.DS.'libraries'.DS.'loader.php');
	}
	phocagalleryimport('phocagallery.file.filethumbnail');
}

class modImageFlowHelper
{

	function getTemplate( $tmplt )
	{
		jimport('joomla.filesystem.file');
		//
		//	load the template file
		//
		$tmpltspec = JPATH_BASE . DS . $tmplt;
		$tmpltspec = JPath::clean( $tmpltspec, DS );
		$tmpltext = '';

		if (JFILE::exists($tmpltspec))
		{
			$tmpltspecs = JFILE::read($tmpltspec);
			$tmpltspecs = str_replace( "\r\n", "\n", $tmpltspecs );
			$tmpltspecs = str_replace( "\r", "\n", $tmpltspecs);
			$tmpltlines = explode( "\n", $tmpltspecs );

			for ($i=0; $i<count($tmpltlines); $i++)
			{
				$tmpltline = JString::ltrim($tmpltlines[$i]);
				if (JString::substr( $tmpltline, 0, 2) != '//')
				{
					$tmpltext .= $tmpltline;
				}
			}
		}
		return $tmpltext;
	}


	function resolveVariables( &$imgline, &$varArray )
	{
		if ( eregi( "[$]([^ \t]+)([ \t])*=([ \t])*\"([^\"]+)\"", $imgline, $match ) > 0 )
		{
			do
			{
				modImageFlowHelper::substituteVariables( $match[4], $varArray );
				$varArray[$match[1]] = $match[4];
				$imgline = str_replace( $match[0], '', $imgline);
			}
			while( eregi( "[$]([^ \t]+)([ \t])*=([ \t])*\"([^\"]+)\"", $imgline, $match ) > 0 );

			return FALSE;
		}

		return modImageFlowHelper::substituteVariables( $imgline, $varArray );
	}

	function substituteVariables( &$imgline, &$varArray )
	{
		$keys = array_keys( $varArray );
		for( $i = 0; $i < count( $keys ); $i++)
		{
			$imgline = str_replace( '{'.$keys[$i].'}', $varArray[$keys[$i]], $imgline );
		}
		return TRUE;
	}

	function combineLines( $imgspecs )
	{
		$lines = explode( "\n", $imgspecs );
		$imgs = array();

		for ($i=0; $i<count($lines); $i++)
		{
			if (isset($line))
		 	{
				$line .= $lines[$i];
			}
			else
			{
				$line = $lines[$i];
			}
			$line = JString::rtrim( $line, " \t" );
			if (JString::substr( $line, -1) == "&")
			{
				$line = JString::substr( $line, 0, JString::strlen( $line ) - 1 );
			}
			else
			{
				$imgs[] = $line;
				unset( $line );
			}
		}
		if (isset($line))
		{
			$imgs[] = $line;
		}
		return $imgs;
	}

	function getImages($params)
	{
		jimport('joomla.filesystem.file');

		if (file_exists('components'.DS.'com_phocagallery'.DS.'helpers'.DS.'phocagallery.php'))
			$oldversion=1;

		$onlyparents = '';
		if ($params->get('onlyparents'))
			$onlyparents = ' AND cc.parent_id = 0';

		$onlypublic = '';
		if ($params->get('onlypublic'))
			$onlypublic = ' AND cc.access = 0';

		$db		=& JFactory::getDBO();
		$query	= 'SELECT cc.title AS text, cc.id AS id, cc.parent_id as parentid, cc.alias as alias, cc.access as access, cc.params as params, ii.filename as filename'
		. ' FROM #__phocagallery_categories AS cc, #__phocagallery AS ii'
		. ' WHERE cc.published = 1'
		. ' AND cc.id = ii.catid'
		. ' AND ii.ordering = 1 '
		. $onlyparents
		. $onlypublic
		. ' ORDER BY cc.parent_id,cc.ordering ASC';
		$db->setQuery( $query );
		$categoryData = $db->loadObjectList();

		$i=0;
		if(isset($categoryData) && !empty($categoryData)) {
			foreach ($categoryData as $key => $value) {
				$images[$i]['id']  = "imageflow_image_".$value->id;
				if ($oldversion) {
					$file_thumbnail	= PhocaGalleryHelperFront::getThumbnailName($value->filename, $params->get('thumbnailsize'));
					$images[$i]['img'] = $file_thumbnail['rel'];
				}
				else {
					$file_thumbnail	= PhocaGalleryFileThumbnail::getThumbnailName($value->filename, $params->get('thumbnailsize'));
					$images[$i]['img'] = $file_thumbnail->rel;
				}
				$images[$i]['imgalt'] = str_replace('&','AND',$value->text);
				$images[$i]['imgtitle'] = str_replace('&','AND',$value->text);
				$images[$i]['hreftitle'] = str_replace('&','AND',$value->text);
				$images[$i]['url'] = modImageFlowHelper::getLink($value->id);
				//$images[$i]['url'] = JRoute::_('index.php?option=com_phocagallery&view=category&id='. $value->id.':'.$value->alias.'&Itemid=51'); //. (JRequest::getVar('Itemid', 1, 'get', 'int')) );
				$images[$i]['tmplt'] = "modules/mod_em_phocaflow/templates/urllinktemplate.txt";
				$i++;
			}
		}

		if (count($images) == 0 )
		{
			$path_relative = str_replace(JPATH_BASE . DS, '', dirname(__FILE__) );
			$path_relative = JPath::clean( $path_relative, '/');
			$images[0]['img'] = $path_relative.'/images/qmark.jpg';
			$images[0]['imgalt'] = 'No images found!';
			$images[0]['imgtitle'] = 'No images found!';
			$images[0]['hreftitle'] = 'No images found!';
		}
		return $images;
	}

	// Create link (set Itemid)
	function getLink( $id )
	{
		// Set Itemid id, exists this link in Menu?
		$menu          = &JSite::getMenu();
		$itemsCategory   = $menu->getItems('link', 'index.php?option=com_phocagallery&view=category&id='.(int) $id );
		$itemsCategories= $menu->getItems('link', 'index.php?option=com_phocagallery&view=categories');

		if(isset($itemsCategory[0])) {
			$itemId = $itemsCategory[0]->id;
			$alias    = $itemsCategory[0]->alias;
		}
		else if(isset($itemsCategories[0])) {
			$itemId = $itemsCategories[0]->id;
			$alias    = '';
   		}
		else {
			$itemId = 0;
			$alias   = '';
		}

		if ($alias != '') {
			$catid_slug = $id . ':' . $alias;
		}
		else {
			$db    =& JFactory::getDBO();
			$query    = 'SELECT c.alias'.
			' FROM #__phocagallery_categories AS c' .
			' WHERE c.id = '. (int)$id;
			$db->setQuery( $query );
			$catid_alias = $db->loadObject();

			if (isset($catid_alias->alias) && $catid_alias->alias != '') {
				$catid_slug = (int)$id . ':'.$catid_alias->alias;
			}
			else {
				$catid_slug = (int)$id;
			}
		}
		// Category
		$link = JRoute::_('index.php?option=com_phocagallery&view=category&id='. $catid_slug .'&Itemid='.$itemId);
		return ($link);
	}


}