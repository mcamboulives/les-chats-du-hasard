<?php 
/*
// "Easy Flexi Module" Module by CCC for Joomla! and Flexicontent 1.5.x
// Copyright (c) 2006 - 2010 ccomca.com All rights reserved.
// Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
// More info at http://www.ccomca.com/communaute/item/314-easy-flexi-module.html
// Designed and developed by the CCC team
// *** Last update: 29/04/2010 ***
*/

// no direct access
defined('_JEXEC') or die('Restricted access'); 

//add css
if ($params->get('complex_css', 0) == 0)
	{
	$doc = &JFactory::getDocument();
	$doc->addStyleSheet(JURI::Root(true) . '/modules/mod_easyfleximodule/assets/css/complex.css');
	}

?>
<?php foreach ($list as $item) : ?>

<div class="item">
	<?php print_r ($item); ?>
  <?php if ($item->src) : ?>
 	<?php echo $item->image_principale; ?>

    <?php if ($params->get('link_image', 1)) : ?>
    <a href="<?php echo $item->link; ?>" alt="<?php echo $item->text; ?>"> <img src="<?php echo $item->thumb; ?>" alt="<?php echo $item->text; ?>" class="<?php echo $params->get('image_position') == 1 ? 'thumbright' : 'thumbleft'; ?>" /> </a>
    <?php else : ?>
    <img src="<?php echo $item->thumb; ?>" alt="<?php echo $item->text; ?>" style="float: <?php echo $params->get('image_position') == 1 ? ' right' : ' left'; ?>;"/>
    <?php endif; ?>
  
  <?php endif; ?>
  <p class="content">
  <span class="title"> <?php if ($item->date) : ?> <span class="date"><?php echo $item->date; ?></span> <?php endif; ?> <a href="<?php echo $item->link; ?>" class="mostread<?php echo $params->get('moduleclass_sfx'); ?>"> <?php echo $item->text; ?></a></span>
  <br />
  <span class="description"> <?php echo $item->description; ?> </span>
  <?php if ($params->get('show_readmore', 0) == 0) : ?>
  <span class="readmore"> <a href="<?php echo $item->link; ?>" class="readon"> <?php echo ' ' . JText::sprintf('Read More', $items[$i]->title); ?> </a> </span>
  <?php endif; ?>
  </p>
</div>
<?php endforeach; ?>