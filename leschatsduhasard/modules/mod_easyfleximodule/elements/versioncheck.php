<?php
/*
// "Easy Flexi Module" Module by CCC for Joomla! and Flexicontent 1.5.x
// Copyright (c) 2006 - 2010 ccomca.com All rights reserved.
// Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
// More info at http://www.ccomca.com/communaute/item/314-easy-flexi-module.html
// Designed and developed by the CCC team
// *** Last update: 02/11/2010 ***
*/
defined('JPATH_BASE') or die();

class JElementVersionCheck extends JElement
{
	var	$_name = 'VersionCheck';
    var $_error = null;

	function fetchElement($name, $value, &$node, $control_name)
	{
		
		$cache = & JFactory::getCache('mod_easyfleximodule');
		$cache->setCaching( 1 );
		$cache->setLifeTime( 100 );
		$check = $cache->get(array( 'JElementVersionCheck', 'getUpdateModule'), 'module');
		
		if ($check['connect'] == 0)
			{
				$msg ='<div style="background: #FFD5D5;color: #ff0000;padding:5px"> <img src="'.JURI::root(true).'/modules/mod_easyfleximodule/assets/images/alert.png" alt ="" align="absmiddle" vspace="5" /> '.JText::_( 'CONNECTION FAILED' ).'</div>';
			}
			else
			{
				if ($check['current'] == 0) {
		  				$msg ='<div style="background: #e4f3e2;color: #000;padding:5px"> <img src="'.JURI::root(true).'/modules/mod_easyfleximodule/assets/images/approved.png" alt ="" align="absmiddle" vspace="5" /> '.sprintf ( JText::_( 'LATEST VERSION' ),  $check['version'], $check['current_version'], $check['link']).'</div>';
		  			} else {
		  				$msg ='<div style="background: #FFD5D5;color: #ff0000;padding:5px"> <img src="'.JURI::root(true).'/modules/mod_easyfleximodule/assets/images/alert.png" alt ="" align="absmiddle" vspace="5" /> '.sprintf ( JText::_( 'NEW VERSION' ),  $check['version'], $check['current_version'], $check['link']).'</div>';
		  			}
				
			}
			return $msg;
	}
	
	function getUpdateModule()
	 {
	 	$url = 'http://code.google.com/feeds/p/easyfleximodule/downloads/basic';
		$data = '';
		$check = array();
		$check['connect'] = 0;
		
		$mod_xml 		= JApplicationHelper::parseXMLInstallFile( JPATH_SITE .DS. 'modules' .DS. 'mod_easyfleximodule' .DS. 'mod_easyfleximodule.xml' );
		$check['current_version'] = $mod_xml['version'];

		//try to connect via cURL
		if(function_exists('curl_init') && function_exists('curl_exec')) {		
			$ch = @curl_init();
			
			@curl_setopt($ch, CURLOPT_URL, $url);
			@curl_setopt($ch, CURLOPT_HEADER, 0);
			//http code is greater than or equal to 300 ->fail
			@curl_setopt($ch, CURLOPT_FAILONERROR, 1);
			@curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//timeout of 5s just in case
			@curl_setopt($ch, CURLOPT_TIMEOUT, 5);
						
			$data = @curl_exec($ch);
						
			@curl_close($ch);
		}

		
						
		if( $data ) {
			$xml = & JFactory::getXMLparser('Simple');
			$xml->loadString($data);
			$version = '';
			foreach ($xml->document->entry as $entry) {
				
				$title = (string) $entry->title[0]->data();
				if (preg_match('/easyfleximodule-(.*).zip/', $title, $matches)){
						   
							$version = $matches[1];
				}
				$released 				= & $entry->updated[0];
				$check['released'] 		= & $released->data();
				$linkattribs 			= & $entry->link[0]->attributes();
				$check['link'] 			= & $linkattribs['href'];
			}
			
			
			$check['version'] 		= $version;
			$check['connect'] 		= 1;
			$check['enabled'] 		= 1;
			
			$check['current'] 		= version_compare( $check['current_version'], $check['version'] );
		}
		
		return $check;
	 }

}