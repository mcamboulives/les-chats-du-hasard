<?php
/**
 *
 * BlockDisposable
 *
 * @version $Id: cedit_blockdisposable.php $
 * @package cedit_blockdisposable
 * @copyright Copyright (C) 2009 David Barrett. All rights reserved.
 * @website http://www.cedit.biz
 * @license GNU/GPL
 *
 */

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.event.plugin');

class plgSystemCedit_BlockDisposable extends JPlugin {
	var $_plugin;
	var $_params;
	var $logFileHandle;
	var $logFileOpen;
	var $loggingEnabled;
	var $debug;

	/**
	* Constructor
	*
	* @return plgSystemCedit_BlockDisposable
	*/
	function plgSystemCedit_BlockDisposable(& $subject){
		parent::__construct($subject);
		//load plugin parameters
		$this->_plugin =  JPluginHelper::getPlugin('system','cedit_blockdisposable');
		$this->_params = new JParameter($this->_plugin->params);
		$this->debug = FALSE;
	} // end plgSystemCedit_BlockDisposable
	
	function OpenLogFile() {
		$logFile = $this->_params->get( 'logfile' , '' );
		$this->loggingEnabled = FALSE;
		if ( $logFile != '' ) {
			$logFile = $_SERVER['DOCUMENT_ROOT'] . $logFile;
			if ( !file_exists( $logFile ) ) {
				$this->logFileHandle = @fopen( $logFile, "w" );
			} else {
				$this->logFileHandle = @fopen( $logFile, "a" );
			}
			if ( $this->logFileHandle !== FALSE ) {
				$this->logFileOpen = TRUE;
				$this->loggingEnabled = TRUE;
			}
		}
	}
	
	function CloseLogFile() {
		if ( !$this->logFileOpen ) return;
		plgSystemCedit_BlockDisposable::Log( '------------------------' );
		fclose( $this->logFileHandle );
		$this->logFileOpen = FALSE;
	}
	
	function Log( $data ) {
		// Write data to the log file
		if ( !$this->loggingEnabled ) return;
		$time = date( 'd/m/Y H:i:s' );
		$log = $time . '  ' . $data . "\r\n";

		fwrite( $this->logFileHandle, $log );
	}
	
	function onAfterInitialise() {
		// We need to check here for TPLancers (it bypasses Joomla events)
		$option = strtolower (strval(  JRequest::getVar('option' )));
		if ( $option != 'com_tplancers' ) return;
		$task = strtolower (strval(  JRequest::getVar( 'task' ) ) );
		$func = strtolower (strval( JRequest::getVar( 'func' )));
		$this->ValidateRegistration( $option, $task, $func );
	}

	function onAfterRoute() {
		$option = strtolower (strval(  JRequest::getVar('option' )));
		$task = strtolower (strval(  JRequest::getVar( 'task' ) ) );
		$func = strtolower (strval( JRequest::getVar( 'func' )));
		$this->ValidateRegistration( $option, $task, $func );
	}
	
	function ValidateRegistration( $option, $task, $func ) {
		global $mainframe;
		$option = strtolower (strval(  JRequest::getVar('option' )));
		$task = strtolower (strval(  JRequest::getVar( 'task' ) ) );
		$func = strtolower (strval( JRequest::getVar( 'func' )));
		$isregistering = false;
		$jomsocial = false;
		$contactForm = false;
		
		plgSystemCedit_BlockDisposable::OpenLogFile();
		//plgSystemCedit_BlockDisposable::Log( 'Option = ' . $option . ', Task = ' . $task . ', Func = ' . $func );
		$redirect = $this->_params->get('redirectaddress','');
		
		
		// Check registration variables from various components
		if ( $option == 'com_user' && $task == 'register_save' ) $isregistering = true;
		if ( $option == 'com_comprofiler' && $task == 'saveregisters' ) $isregistering = true;
		if ( $option == 'com_cbe' && $task == 'saveregistration' ) $isregistering = true;
		if ( $option == 'com_ajaxregistration' && $task == 'register_save' ) $isregistering = true;
		if ( $option == 'com_virtuemart' && $func=='shopperadd' ) {
			$user =& JFactory::getUser();
			if ( $user->get('username') != '' ) return true;
			$isregistering = true;
		}
		if ( $option == 'com_tplancers' && $task == 'savebuyernew' ) $isregistering = true;
		if ( $option == 'com_tplancers' && $task == 'savelancernew' ) $isregistering = true;
		if ( $option == 'com_community' && $task == 'register_save' ) {
			$isregistering = true;
			$jomsocial = true;
		}
		
		// Check for contact form submission
		if ( $this->_params->get('protectContactForms','1') > 0 ) {
			if ( $option == 'com_contact' && $task == 'submit' ) $contactForm = true;
		}

		if ( !$isregistering && !$contactForm ) return true;
		plgSystemCedit_BlockDisposable::Log( 'Registration Validator Start' );
		

		if ( $jomsocial ) {
			$email = strtolower ( strval(  JRequest::getVar( 'jsemail' ) ) );
			$username = strtolower ( strval(  JRequest::getVar( 'jsusername' ) ) );
		} else {
			$email = strtolower ( strval(  JRequest::getVar( 'email' ) ) );
			$username = strtolower ( strval(  JRequest::getVar( 'username' ) ) );
		}
		$ip = plgSystemCedit_BlockDisposable::getClientIP();
		plgSystemCedit_BlockDisposable::Log( 'Email = ' . $email . ', IP = ' . $ip . ', username = ' . $username );
		
		$canRegister = true;
		$blockReason = '';

		// Check whether IP address is blocked
		plgSystemCedit_BlockDisposable::Log( 'Checking local IP blocklist' );
		if (plgSystemCedit_BlockDisposable::isIPBlocked() ) {
			// IP matched a blocked IP entry
			$canRegister = false;
			$blockReason = 'IP address blocked';
			plgSystemCedit_BlockDisposable::Log( 'Registration blocked by local IP blocklist' );
		}         // end if
		plgSystemCedit_BlockDisposable::Log( 'Finished checking local IP blocklist' );

		if ( $canRegister ) {
			plgSystemCedit_BlockDisposable::Log( 'Checking Spamhaus blocklist' );
			$spamhausresult = plgSystemCedit_BlockDisposable::isSpamhausListed( $ip );
			if ( $spamhausresult !== false ) {
				$canRegister = false;
				$blockReason = 'IP listed in Spamhaus (' . $spamhausresult . ')';
				plgSystemCedit_BlockDisposable::Log( 'IP address blocked by Spamhaus' );
			}
			plgSystemCedit_BlockDisposable::Log( 'Finished checking Spamhaus blocklist' );
		}
		
		if ( $canRegister  && !$contactForm ) {
			// IP address ok, check whether username is blocked
			plgSystemCedit_BlockDisposable::Log( 'Checking local username blocklist' );
			if ( plgSystemCedit_BlockDisposable::isUserNameBlocked( $username ) ){
				$canRegister = false;
				$blockReason = "Invalid username";
				plgSystemCedit_BlockDisposable::Log( 'Registration blocked by local username blocklist' );
			}
			plgSystemCedit_BlockDisposable::Log( 'Finished checking local username blocklist' );
		}

		if ( $canRegister  && $email!='' ) {
			// Check email address internally
			$checkDNS = (int) $this->_params->get('validateDNS',1);
			plgSystemCedit_BlockDisposable::Log( 'Checking local email blocklist' );
			if ( plgSystemCedit_BlockDisposable::LocalCheckIsDisposable( $email, $checkDNS ) ) {
				$canRegister = false;
				$blockReason = "Email address or domain is on internal block list";
				plgSystemCedit_BlockDisposable::Log( 'Registration blocked by local email blocklist' );
			} else {
				$usedisposable = $this->_params->get('usedisposable',1);
				if ($usedisposable!=0) {
					// Ask undisposable whether email is disposable...
					plgSystemCedit_BlockDisposable::Log( 'Checking Undisposable blocklist' );
					if (plgSystemCedit_BlockDisposable::isUndisposableListed( $email ) ) {
						$canRegister=false;
						$blockReason = "Email domain is disposable mail host";
						plgSystemCedit_BlockDisposable::Log( 'Registration blocked by Undisposable' );
					}
				}
			}
			plgSystemCedit_BlockDisposable::Log( 'Finished checking local email and Undisposable blocklist' );
		}

		if ( $canRegister && $email != '') {
			plgSystemCedit_BlockDisposable::Log( 'Checking Botscout blocklist' );
			$checkBotScout = (int) $this->_params->get('useBotScout',1);
			if ($checkBotScout) {
				if ( plgSystemCedit_BlockDisposable::isBotScoutListed( $email, $ip ) ) {
					$canRegister = false;
					$blockReason = "IP and/or email blocked by BotScout";
					plgSystemCedit_BlockDisposable::Log( 'Registration blocked by Botscout' );
				}
			}
			plgSystemCedit_BlockDisposable::Log( 'Finished checking Botscout blocklist' );
		}

		if ( $canRegister && $email != '' ) {
			plgSystemCedit_BlockDisposable::Log( 'Checking StopForumSpam blocklist' );
			if ( plgSystemCedit_BlockDisposable::isStopForumSpamListed( $email, $ip ) ) {
				$canRegister = false;
				$blockReason = 'IP and/or email blocked by StopForumSpam';
				plgSystemCedit_BlockDisposable::Log( 'Registration blocked by StopForumSpam' );
			}
			plgSystemCedit_BlockDisposable::Log( 'Finished checking StopForumSpam blocklist' );
		}
		
		if ( $canRegister  && $contactForm && $email != '' ) {
			// Some extra contact form checks, just blocks some common spam methods
			plgSystemCedit_BlockDisposable::Log( 'Checking contact form submission' );
			if ( plgSystemCedit_BlockDisposable::isContactSubmissionDubious( $email ) ) {
				$canRegister = false;
				$blockReason = "Contact submission is spam";
				plgSystemCedit_BlockDisposable::Log( 'Contact form blocked by email check fail' );
			}
			if ( plgSystemCedit_BlockDisposable::contactSubmissionContainsLinks() ) {
				$canRegister = false;
				$blockReason = "Links are not allowed in contact form submissions";
				plgSystemCedit_BlockDisposable::Log( 'Contact form blocked due to links' );
			}
			plgSystemCedit_BlockDisposable::Log( 'Finished checking contact form submission' );			
		}

		if (!$canRegister) {
			$session =& JFactory::getSession();
			$session->set( 'registrationvalidatorfail', $blockreason );
			if ( !$contactForm ) {
				// Registration blocked
				
				$msg = $this->_params->get( 'emailmsg', 'Sorry, the registration failed.  Reason: ');
				if ( $msg[ strlen( $msg ) - 1 ] == ':' ) $msg .= ' '; // Replace trimmed space if colon present at end
				$msg .= $blockReason;
				$dlmsg = sprintf($msg,$email);
				plgSystemCedit_BlockDisposable::Log('REGISTRATION BLOCKED: ' . $blockReason);
				
				$session->set( 'registrationvalidatorfail', $dlmsg );
				
				if ( $redirect == '' ) {
					switch ( $option ) {
						case 'com_ajaxregistration':
							$redirect = 'index.php?option=com_ajaxregistration&task=register';
							break;
						case 'com_comprofiler' :
							plgSystemCedit_BlockDisposable::Log( 'Redirecting to CB registration page');
							plgSystemCedit_BlockDisposable::CloseLogFile();
							$mainframe->redirect("index.php?option=com_comprofiler&task=registers",$dlmsg,$dlmsg);
							break;
						case 'com_cbe':
							$redirect = "index.php?option=com_cbe&task=registers";
							break;
						case 'com_user':
							$redirect = "index.php?option=com_user&task=register";
							break;
						case 'com_virtuemart':
							$redirect = 'index.php?option=com_virtuemart&amp;page=shop.registration';
							break;
						case 'com_community':
							$redirect = 'index.php?option=com_community&view=register';
							break;
					}
				}
					
				if ($redirect!='') {
					plgSystemCedit_BlockDisposable::Log( 'Redirecting to ' . $redirect);
					JRequest::setVar( 'task', '' );
					plgSystemCedit_BlockDisposable::CloseLogFile();
					$mainframe->redirect($redirect,$dlmsg);
				} else {
				}
			} else {
				// Contact form submission blocked
				JRequest::setVar( 'task', '' );
				if ( $redirect == '' ) {
					$redirect = "index.php?option=com_contact&view=contact&id=" . JRequest::getVar('id' ) . "&Itemid=" . JRequest::getVar('Itemid' );
				}
				plgSystemCedit_BlockDisposable::Log( 'Contact form blocked - redirecting to ' . $redirect);
				plgSystemCedit_BlockDisposable::CloseLogFile();
				$mainframe->redirect( $redirect, $blockReason );
			}
		} else {
			//This address is ok
			plgSystemCedit_BlockDisposable::Log( 'Registration Validator found no problems' );
		} // end if
		plgSystemCedit_BlockDisposable::Log( 'Registration Validator finished' );
		plgSystemCedit_BlockDisposable::CloseLogFile();
	} // end onBeforeStoreUser
	
	function onBeforeDisplayContent( &$article, &$params, $limitstart ) {
		// Here we are adding a Javascript popup if there is a registration validator message to show
		$showPopup = (int) $this->_params->get('showpopuponfailure',0);
		if ( $showPopup == 0 ) return '';

		// Check  whether we have session failure report 
		$session =& JFactory::getSession();
		$failed = $session->get( 'registrationvalidatorfail' );
		if ( isset( $failed ) ) {
			$session->clear( 'registrationvalidatorfail' );
		} else return '';
		$html = "<SCRIPT>alert('$failed');</SCRIPT>";
		return $html;
	}

	function LocalCheckIsDisposable( $email, $validate ) {
		$blocklist = $this->_params->get('blocklist','');
		if ($blocklist == '') {
			include('defaultblocklist.php');
		}
		$blockeddomains = explode( ',', $blocklist);
		$domain = plgSystemCedit_BlockDisposable::get_domain_from_address( $email );
		$d = explode( '.', $domain );
		$i = count( $d ) - 1;
		while ( $i >= 0 ) {
			$checkdomain = '';
			for ( $j = $i; $j <= count( $d ) - 1; $j++ ) {
				if ( $j > 0 ) $checkdomain .= '.';
				$checkdomain .= $d[$j];
			}
			if ( in_array( $checkdomain, $blockeddomains ) ) return true;
			$i--;
		}
		if ( in_array( '.' . $domain, $blockeddomains ) ) return true;

		if ( $validate ) {
			// MX check the email domain
			if ( function_exists( 'getmxrr' ) ) {
				if ( getmxrr( $domain, $mxhosts ) === FALSE ) {
					// No MX records - need to check A RR (as described in RFC2821)
					if ( checkdnsrr( $domain, 'A' ) !== FALSE ) {
						// We have a valid MX
						return FALSE;
					}
					return TRUE;
				}
			}
		}
		return false;
	}

	function get_domain_from_address($email) {
		$domain_pos = strpos($email, '@');
		if ( $domain_pos === false ) {
			return '';
		}
		return strtolower( substr( $email, $domain_pos + 1 ) );
	}

	function isUndisposableListed( $email ) {
		// Ask undisposable whether email is disposable...
		$url = "http://www.undisposable.net/services/php/";
		$url .= "isDisposableEmail/?email=".addslashes($email);
		$res = plgSystemCedit_BlockDisposable::getURL( $url );
		if ( !$res ) return false;  // If no response, or error, return
		$uns = @unserialize($res);
		if($uns['stat']=='ok') {
			// We got a valid reply, check the result
			if ($uns['email']['isdisposable']) {
				return true;
			}
		}
		return false;
	}
	
	function getURL( $uri ) {
		// Returns the data from the given URL
		$timeout = (int)$this->_params->get('httptimeout','5');
		if ( $timeout < 1 ) $timeout = 1;
		$ch = curl_init($uri);
		curl_setopt( $ch, CURLOPT_HEADER, 0 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout ); // Set timeout
		curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout ); // Set timout
		$data = curl_exec($ch);
		if ( !$data ) {
			// Connection failed, log this with reason
			$this->Log( 'Error in connection: ' . curl_error( $ch ) );
		}
		curl_close($ch);
		return $data;
	}

	function isBotScoutListed( $email, $ip ) {
		$apikey = $this->_params->get('apikey','');
		$email = urlencode($email);
		$querystr = "http://botscout.com/test/?multi&mail=$email&ip=$ip";
		if ($apikey) $querystr .= "&key=$apikey";
		if ( $this->debug ) $this->Log( 'Botscout query = ' . $querystr );
		
		$data = plgSystemCedit_BlockDisposable::getURL( $querystr );
		if ( !$data ) return false;
		if ( $this->debug ) $this->Log( 'Botscout reply = ' . $data );
		$botdata = explode('|', $data);

		// Error (!) or not listed (N) --> return false
		if ( substr($data, 0, 1) == '!' || substr($data, 0,1) == 'N' ) return false;

		// IP address or email address listed on botscout --> return true
		if ( $botdata[3] > 0 || $botdata[5] > 0) {
			return true;
		}
		return false;
	}

	function isSpamhausListed( $ip ) {
		// Check the IP address against sbl.spamhaus.org and xbl.spamhaus.org
		// We don't want to use zen, as we don't want to block dynamic IP ranges - this is NOT an email server check
		// Also, am going to specifically check return result for 127.x.x.x, as some ISPs returns an incorrect DNS
		// result for spamhaus.org lookups
		if ( $this->_params->get( 'checkspamhaus' , '1' ) == 0 ) return false;
		$reverseIPOctets = array_reverse( explode( '.', $ip ) );
		$reverseIP = implode( '.',  $reverseIPOctets);
		// Check SBL
		$sblLookup = $reverseIP . '.sbl.spamhaus.org.';
		$result = gethostbyname( $sblLookup );
		if ( $result != $sblLookup ) {
			$result = explode( '.', $result );
			if ( ( $result[0] == '127' ) ) return true;
		}
		// Check XBL
		$xblLookup = $reverseIP . '.xbl.spamhaus.org.';
		$result = gethostbyname( $xblLookup );
		if ( $result != $xblLookup ) {
			$result = explode( '.', $result );
			if ( $result[0] == '127' ) return true;
		}
		return false;
	}

	function isStopForumSpamListed( $email, $ip ) {
		if ( $this->_params->get('usestopforumspam','1') == 0) return false;
		
		// Query StopForumSpam
		$email = urlencode( $email );
		$querystr = 'http://www.stopforumspam.com/api?email=' . $email .'&ip=' . $ip;
		if ( $this->debug ) plgSystemCedit_BlockDisposable::Log( 'StopForumSpam query = ' . $querystr );
		$data = plgSystemCedit_BlockDisposable::getURL( $querystr );

		if ( !$data ) return false;		
		if ( $this->debug ) {
			// Log response - but get rid of newlines first
			$logdata = str_replace(	"\n", "", $data );
			$logdata = str_replace(	"\r", "", $logdata );
			plgSystemCedit_BlockDisposable::Log( 'StopForumSpam result = ' . $logdata );
		}
		
		// Now check StopForumSpam response
		$emailbad = false;
		$ipbad = false;
		$count = 0;
		if ( class_exists( 'SimpleXMLElement' ) ) {
			// SimpleXML is available, so use it to process response
			if ( $this->debug ) plgSystemCedit_BlockDisposable::Log( 'Using SimpleXMLElement' );
			if ( function_exists('libxml_use_internal_errors') ) libxml_use_internal_errors( true );
			$xml = new SimpleXMLElement( $data );
			if ( count( libxml_get_errors() ) >0 ) {
				// We had a problem parsing the XML... disregard the result
				if ( $this->debug ) plgSystemCedit_BlockDisposable::Log( 'SimpleXML Errors, result ignored' );
			} else {
				$emailbad = ( $xml->appears[0]=='yes' );
				$ipbad = ( $xml->appears[1]=='yes' );
				if ( $this->debug ) plgSystemCedit_BlockDisposable::Log( 'Email bad = ' . $emailbad . ', IP bad = ' . $ipbad );
			}
			libxml_clear_errors();
		}else{
			// SimpleXML not available, just use basic checking
			$count = substr_count( $data, '<appears>yes</appears>' );
			if ( $this->debug ) plgSystemCedit_BlockDisposable::Log( 'Basic analysis - matches = ' . $count );
		}
		
		// Now return result depending upon plugin parameters
		if ( $this->_params->get('usestopforumspam','1') == 1) {
			// Block if ANY match
			if ( $emailbad || $ipbad || ( $count > 0 ) ) return true;
		} else {
			// Block only if all details match
			if ( ( $emailbad && $ipbad ) || ( $count == 2 ) ) return true;
		}
		if ( $this->debug ) plgSystemCedit_BlockDisposable::Log( 'StopForumSpam check result ok' );
		
		return false;
	}

	function getClientIP() {
		if ( isset( $_SERVER["HTTP_X_FORWARDED_FOR"] ) ) {
			if ( $_SERVER["HTTP_X_FORWARDED_FOR"] ) return $_SERVER["HTTP_X_FORWARDED_FOR"];
		}
		if ( isset( $_ENV["HTTP_CLIENT_IP"] ) ) {
			if ($_ENV["HTTP_CLIENT_IP"]) return $_ENV["HTTP_CLIENT_IP"];
		}
		if ( isset( $_SERVER["REMOTE_ADDR"] ) ) {
			if ($_SERVER["REMOTE_ADDR"]) return $_SERVER["REMOTE_ADDR"];
		}
		return '0.0.0.0';
	}

	function isIPBlocked() {
		$blockedips = $this->_params->get( 'blockedips', '' );
		if ( $blockedips != '' ) {
			$ip = plgSystemCedit_BlockDisposable::getClientIP();
			$octets = explode( '.', $ip );
			$regex = '';
			foreach ( $octets as $octet ) {
				if ( $regex!='' ) $regex .= '\.';
				$regex .= '(\*|' . $octet . ')';
			}
			$regex = '/' . $regex . '/';
			if ( preg_match($regex, $blockedips) ) {
				// IP matched a blocked IP entry
				return true;
			}
		}         // end if
		return false;
	}

	function isUsernameBlocked( $username ) {
		$blockednames = explode(',',$this->_params->get('blockednames',''));
		if($blockednames!='') {
			$n = count($blockednames);
			for ($i=0;$i<$n;$i++){
				if ($blockednames[$i]!='') {
					$blockednames[$i] = trim($blockednames[$i]);
					$blockednames[$i] = preg_quote($blockednames[$i], '#');
					$blockednames[$i] = str_replace(array("\r\<br /\>", '\*'), array('|', '.*'), $blockednames[$i] );
					$regex[$i] = "#^(?:$blockednames[$i])$#i";
					if(preg_match($regex[$i], $username)){
						return true;
					}
				}
			} // end for
		} // end if
		return false;
	}
		  
	function isContactSubmissionDubious( $email ) {
		if ( $this->_params->get('protectContactForms','1') != 2 ) return false;
		$domain_pos = strpos( $email, '@' );
		$left = strtolower( substr( $email, 0, $domain_pos) );
		$right = substr( $email, $domain_pos + 1);
		$period_pos = strpos( $right, '.' );
		$right = strtolower( substr( $right, 0, $period_pos ) );
		if ( $left==$right ) return true;
		return false;
	}
	
	function contactSubmissionContainsLinks() {
		if ( $this->_params->get('rejectLinks','1') > 0 ) {
			// Check there are no links in the submission
			$text = JRequest::getVar( 'text' );
			if ( stripos( $text, 'http:' ) !== FALSE ) return TRUE;
			if ( stripos( $text, 'www.' ) !== FALSE ) return TRUE;
		}
		return false;
	}
}
?>