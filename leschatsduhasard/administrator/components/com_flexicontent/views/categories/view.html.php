<?php
/**
 * @version 1.5 stable $Id: view.html.php 1750 2013-09-03 20:50:59Z ggppdk $
 * @package Joomla
 * @subpackage FLEXIcontent
 * @copyright (C) 2009 Emmanuel Danan - www.vistamedia.fr
 * @license GNU/GPL v2
 * 
 * FLEXIcontent is a derivative work of the excellent QuickFAQ component
 * @copyright (C) 2008 Christoph Lukes
 * see www.schlu.net for more information
 *
 * FLEXIcontent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');

/**
 * View class for the FLEXIcontent categories screen
 *
 * @package Joomla
 * @subpackage FLEXIcontent
 * @since 1.0
 */
class FlexicontentViewCategories extends JViewLegacy
{
	function display($tpl = null)
	{
		//initialise variables
		global $globalcats;
		$app    = JFactory::getApplication();
		$option = JRequest::getVar('option');
		$view   = JRequest::getVar('view');
		$user = JFactory::getUser();
		$db   = JFactory::getDBO();
		$document	= JFactory::getDocument();
		
		JHTML::_('behavior.tooltip');

		//get vars
		$order_property = !FLEXI_J16GE ? 'c.ordering' : 'c.lft';
		$filter_order     = $app->getUserStateFromRequest( $option.'.'.$view.'.filter_order',     'filter_order',     $order_property, 'cmd' );
		$filter_order_Dir = $app->getUserStateFromRequest( $option.'.'.$view.'.filter_order_Dir', 'filter_order_Dir', '', 'word' );
		$filter_state     = $app->getUserStateFromRequest( $option.'.'.$view.'.filter_state',     'filter_state',     '', 'string' );
		$filter_cats      = $app->getUserStateFromRequest( $option.'.'.$view.'.filter_cats',      'filter_cats',			 '', 'int' );
		$filter_level     = $app->getUserStateFromRequest( $option.'.'.$view.'.filter_level',     'filter_level',     '', 'string' );
		$filter_access    = $app->getUserStateFromRequest( $option.'.'.$view.'.filter_access',    'filter_access',    '', 'string' );
		if (FLEXI_J16GE) {
			$filter_language	= $app->getUserStateFromRequest( $option.'.'.$view.'.filter_language',  'filter_language',  '', 'string' );
		}
		$search  = $app->getUserStateFromRequest( $option.'.'.$view.'.search', 			'search', 			'', 'string' );
		$search  = FLEXI_J16GE ? $db->escape( trim(JString::strtolower( $search ) ) ) : $db->getEscaped( trim(JString::strtolower( $search ) ) );

		// Prepare the document: add css files, etc
		$document->addStyleSheet(JURI::base().'components/com_flexicontent/assets/css/flexicontentbackend.css');
		if      (FLEXI_J30GE) $document->addStyleSheet(JURI::base().'components/com_flexicontent/assets/css/j3x.css');
		else if (FLEXI_J16GE) $document->addStyleSheet(JURI::base().'components/com_flexicontent/assets/css/j25.css');
		else                  $document->addStyleSheet(JURI::base().'components/com_flexicontent/assets/css/j15.css');
		
		// Get User's Global Permissions
		$perms = FlexicontentHelperPerm::getPerm();
		
		// Create Submenu (and also check access to current view)
		FLEXISubmenu('CanCats');
		
		
		// ******************
		// Create the toolbar
		// ******************
		$js = "window.addEvent('domready', function(){";
		
		$contrl = FLEXI_J16GE ? "categories." : "";
		$contrl_singular = FLEXI_J16GE ? "category." : "";
		JToolBarHelper::title( JText::_( 'FLEXI_CATEGORIES' ), 'fc_categories' );
		$toolbar = JToolBar::getInstance('toolbar');
		
		// Copy Parameters
		$btn_task = '';
		$popup_load_url = JURI::base().'index.php?option=com_flexicontent&view=categories&layout=params&tmpl=component';
		if (FLEXI_J16GE) {
			$js .= "
				$$('li#toolbar-params a.toolbar, #toolbar-params button')
					.set('onclick', 'javascript:;')
					.set('href', '".$popup_load_url."')
					.set('rel', '{handler: \'iframe\', size: {x: 600, y: 440}, onClose: function() {}}');
			";
			JToolBarHelper::custom( $btn_task, 'params.png', 'params_f2.png', 'FLEXI_COPY_PARAMS', false );
			JHtml::_('behavior.modal', 'li#toolbar-params a.toolbar, #toolbar-params button');
		} else {
			$toolbar->appendButton('Popup', 'params', JText::_('FLEXI_COPY_PARAMS'), $popup_load_url, 600, 440);
		}
		//if (FLEXI_J16GE)
		//	$toolbar->appendButton('Popup', 'move', JText::_('FLEXI_COPY_MOVE'), JURI::base().'index.php?option=com_flexicontent&amp;view=categories&amp;layout=batch&amp;tmpl=component', 800, 440);
		JToolBarHelper::divider();
		
		$add_divider = false;
		if ( !FLEXI_J16GE || ( $user->authorise('core.create', 'com_flexicontent') || (count($user->getAuthorisedCategories('com_content', 'core.create'))) > 0 ) ) {
			JToolBarHelper::addNew($contrl_singular.'add');
			$add_divider = true;
		}
		if ( !FLEXI_J16GE || ( $user->authorise('core.edit', 'com_flexicontent') || $user->authorise('core.edit.own', 'com_flexicontent') ) ) {
			JToolBarHelper::editList($contrl_singular.'edit');
			$add_divider = true;
		}
		
		if ( FLEXI_J16GE && $user->authorise('core.admin', 'checkin') )
		{
			JToolBarHelper::checkin($contrl.'checkin');
			$add_divider = true;
		}
		if ($add_divider) JToolBarHelper::divider();
		
		$add_divider = false;
		if ( !FLEXI_J16GE || ( $user->authorise('core.edit.state', 'com_flexicontent') || $user->authorise('core.edit.state.own', 'com_flexicontent') ) ) {
			JToolBarHelper::publishList($contrl.'publish');
			JToolBarHelper::unpublishList($contrl.'unpublish');
			JToolBarHelper::divider();
			if (FLEXI_J16GE) JToolBarHelper::archiveList($contrl.'archive');
		}
		
		$add_divider = false;
		if ( !FLEXI_J16GE || ( $filter_state == -2 && $user->authorise('core.delete', 'com_flexicontent') ) ) {
			JToolBarHelper::deleteList('Are you sure?', $contrl.'remove');
			//JToolBarHelper::deleteList('', $contrl.'delete', 'JTOOLBAR_EMPTY_TRASH');
			$add_divider = true;
		}
		elseif ( $user->authorise('core.edit.state', 'com_flexicontent') ) {
			JToolBarHelper::trash($contrl.'trash');
			$add_divider = true;
		}
		if ($add_divider) JToolBarHelper::divider();
		
		if ($perms->CanConfig) {
			//JToolBarHelper::custom($contrl.'rebuild', 'refresh.png', 'refresh_f2.png', 'JTOOLBAR_REBUILD', false);
			JToolBarHelper::preferences('com_flexicontent', '550', '850', 'Configuration');
		}
		
		
		$js .= "});";
		$document->addScriptDeclaration($js);
		
		
		//Get data from the model
		if (FLEXI_J16GE) {
			$rows = $this->get( 'Items');
		} else {
			$rows = $this->get( 'Data');
		}
		
		// Parse configuration for every category
   	foreach ($rows as $cat)  $cat->config = FLEXI_J16GE ? new JRegistry($cat->config) : new JParameter($cat->config);
		
		if (FLEXI_J16GE) {
			$this->state = $this->get('State');
			// Preprocess the list of items to find ordering divisions.
			foreach ($rows as &$item) {
				$this->ordering[$item->parent_id][] = $item->id;
			}
		}
		$pagination 	= $this->get( 'Pagination' );
		
		$categories = & $globalcats;
		if (FLEXI_J16GE) {
			$lists['copyid'] = flexicontent_cats::buildcatselect($categories, 'copycid', '', 2, 'class="inputbox"', false, true, $actions_allowed=array('core.edit'));
			$lists['destid'] = flexicontent_cats::buildcatselect($categories, 'destcid[]', '', false, 'class="inputbox" size="15" multiple="true"', false, true, $actions_allowed=array('core.edit'));
		} else if (FLEXI_ACCESS && ($user->gid < 25)) {
			if ((FAccess::checkAllContentAccess('com_content','add','users',$user->gmid,'content','all')) || (FAccess::checkAllContentAccess('com_content','edit','users',$user->gmid,'content','all')) || (FAccess::checkAllContentAccess('com_content','editown','users',$user->gmid,'content','all')) || $CanCats) {
				$lists['copyid'] = flexicontent_cats::buildcatselect($categories, 'copycid', '', 2, 'class="inputbox"', false, false);
				$lists['destid'] = flexicontent_cats::buildcatselect($categories, 'destcid[]', '', false, 'class="inputbox" size="15" multiple="true"', false, false);
			} else {
				$lists['copyid'] = flexicontent_cats::buildcatselect($categories, 'copycid', '', 2, 'class="inputbox"');
				$lists['destid'] = flexicontent_cats::buildcatselect($categories, 'destcid[]', '', false, 'class="inputbox" size="15" multiple="true"');
			}
		} else {
			$lists['copyid'] = flexicontent_cats::buildcatselect($categories, 'copycid', '', 2, 'class="inputbox"');
			$lists['destid'] = flexicontent_cats::buildcatselect($categories, 'destcid[]', '', false, 'class="inputbox" size="15" multiple="true"');
		}
		
		
		// *******************
		// Create Form Filters
		// *******************
		
		// filter by a category (it's subtree will be displayed)
		$categories = $globalcats;
		$lists['cats'] = flexicontent_cats::buildcatselect($categories, 'filter_cats', $filter_cats, 2, 'class="inputbox" size="1" onchange="this.form.submit();"', $check_published=true, $check_perms=false);
		
		// filter depth level
		$options	= array();
		$options[]	= JHtml::_('select.option', '', JText::_( 'FLEXI_SELECT_MAX_DEPTH' ));
		for($i=1; $i<=10; $i++) $options[]	= JHtml::_('select.option', $i, $i);
		$fieldname =  $elementid = 'filter_level';
		$attribs = ' size="1" class="inputbox" onchange="this.form.submit();" ';
		$lists['level']	= JHTML::_('select.genericlist', $options, $fieldname, $attribs, 'value', 'text', $filter_level, $elementid, $translate=true );
		
		// filter publication state
		if (FLEXI_J16GE)
		{
			$options = JHtml::_('jgrid.publishedOptions');
			array_unshift($options, JHtml::_('select.option', '', JText::_('JOPTION_SELECT_PUBLISHED')) );
			$fieldname =  $elementid = 'filter_state';
			$attribs = ' size="1" class="inputbox" onchange="Joomla.submitform()" ';
			$lists['state']	= JHTML::_('select.genericlist', $options, $fieldname, $attribs, 'value', 'text', $filter_state, $elementid, $translate=true );
		} else {
			$lists['state']	= JHTML::_('grid.state', $filter_state );
		}
		
		if (FLEXI_J16GE)
		{
			// filter access level
			$options = JHtml::_('access.assetgroups');
			array_unshift($options, JHtml::_('select.option', '', JText::_('JOPTION_SELECT_ACCESS')) );
			$fieldname =  $elementid = 'filter_access';
			$attribs = ' size="1" class="inputbox" onchange="Joomla.submitform()" ';
			$lists['access']	= JHTML::_('select.genericlist', $options, $fieldname, $attribs, 'value', 'text', $filter_access, $elementid, $translate=true );
			
			// filter language
			$lists['language'] = flexicontent_html::buildlanguageslist('filter_language', 'size="1" class="inputbox" onchange="submitform();"', $filter_language, 2);
		} else {
			// filter access level
			$options = array();
			$options[] = JHtml::_('select.option', '', JText::_('FLEXI_SELECT_ACCESS_LEVEL'));
			$options[] = JHtml::_('select.option', '0', JText::_('Public'));
			$options[] = JHtml::_('select.option', '1', JText::_('Registered'));
			$options[] = JHtml::_('select.option', '2', JText::_('SPECIAL'));
			$fieldname =  $elementid = 'filter_access';
			$attribs = ' size="1" class="inputbox" onchange="this.form.submit()" ';
			$lists['access']	= JHTML::_('select.genericlist', $options, $fieldname, $attribs, 'value', 'text', $filter_access, $elementid, $translate=true );
		}
		
		// filter search word
		$lists['search']= $search;
		
		// table ordering
		$lists['order_Dir'] = $filter_order_Dir;
		$lists['order'] = $filter_order;

		$ordering = ($lists['order'] == $order_property) ? $order_property : '';

		//assign data to template
		$this->assignRef('lists'			, $lists);
		$this->assignRef('rows'				, $rows);
		if (FLEXI_J16GE) {
			$this->assignRef('permission'	, $perms);
			$this->assignRef('orderingx'	, $ordering);
		} else {
			$this->assignRef('CanRights'	, $CanRights);
			$this->assignRef('ordering'		, $ordering);
		}
		$this->assignRef('pagination'		, $pagination);
		$this->assignRef('user'				, $user);

		parent::display($tpl);
	}
}
?>
