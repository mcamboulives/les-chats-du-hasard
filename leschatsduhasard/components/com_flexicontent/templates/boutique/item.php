<?php
/**
 * @version 1.5 stable $Id: item.php 558 2011-03-30 12:18:10Z emmanuel.danan@gmail.com $
 * @package Joomla
 * @subpackage FLEXIcontent
 * @copyright (C) 2009 Emmanuel Danan - www.vistamedia.fr
 * @license GNU/GPL v2
 * 
 * FLEXIcontent is a derivative work of the excellent QuickFAQ component
 * @copyright (C) 2008 Christoph Lukes
 * see www.schlu.net for more information
 *
 * FLEXIcontent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

defined( '_JEXEC' ) or die( 'Restricted access' );
// first define the template name
$tmpl = $this->tmpl;
?>

<div id="flexicontent" class="flexicontent item<?php echo $this->item->id; ?> type<?php echo $this->item->type_id; ?>">

<!-- BOF buttons -->
<p class="buttons">
    <a href="https://twitter.com/share" class="twitter-share-button" data-lang="fr">Tweet</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>		
		<g:plusone size="medium"></g:plusone>
<!-- BOF subtitle1 block -->
<?php if (isset($this->item->positions['subtitle1'])) : ?>
<!--<div class="lineinfo subtitle1">-->
	<?php foreach ($this->item->positions['subtitle1'] as $field) : ?>
	<span class="element">
		<?php if ($field->label) : ?>
		<span class="label field_<?php echo $field->name; ?>"><?php echo $field->label; ?></span>
		<?php endif; ?>
		<span class="value field_<?php echo $field->name; ?>"><?php echo $field->display; ?></span>
	</span>
	<?php endforeach; ?>
<!--</div>-->
<?php endif; ?>
<!-- EOF subtitle1 block -->
	<?php echo flexicontent_html::pdfbutton( $this->item, $this->params ); ?>
	<!--<?php echo flexicontent_html::mailbutton( 'items', $this->params, null , $this->item->slug ); ?>-->
	<?php echo flexicontent_html::printbutton( $this->print_link, $this->params ); ?>
	<?php echo flexicontent_html::editbutton( $this->item, $this->params ); ?>
</p>
<!-- EOF buttons -->

<!-- BOF page title -->
<?php if ($this->params->get( 'show_page_title', 1 ) && $this->params->get('page_title') != $this->item->title) : ?>
<h2 class="componentheading">
	<?php echo $this->params->get('page_title'); ?>
</h2>
<?php endif; ?>
<!-- EOF page title -->
<div class="floattext">
	<h2 class="contentheading flexicontent">
		<?php echo $this->params->get('page_title'); ?>
	</h2>
</div>

<!-- BOF Image block -->
	<div class="image_principale">
		<?php if (isset($this->item->positions['image'])) : ?>
			<?php foreach ($this->item->positions['image'] as $field) : ?>
		<div class="image field_<?php echo $field->name; ?>">
			<?php echo $field->display; ?>
			<div class="clear"></div>
		</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
<!-- EOF Image block -->

<!-- BOF Informations block -->
	<?php if (isset($this->item->positions['informations'])) : ?>
	<div class="informations">
		<div class="content_informations">
			<!-- BOF item title -->
			<?php if ($this->params->get('show_title', 1)) : ?>
			<h2 class="contentheading flexicontent">
				<?php echo $this->escape($this->item->title); ?>
			</h2>
			<?php endif; ?>
			<!-- EOF item title -->
			<ul>
			<?php foreach ($this->item->positions['informations'] as $field) : ?>
				<?php if ($field->label) : ?>
				<li class="desc-title"><span class="label"><?php echo $field->label; ?> : </span><span class="display"><?php echo $field->display; ?></span></li>
				<?php endif; ?>
			<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<?php endif; ?>
<!-- EOF Informations block -->

<!-- BOF Descriptif block -->
	<?php if (isset($this->item->positions['descriptif'])) : ?>
	<div class="description">
		<?php foreach ($this->item->positions['descriptif'] as $field) : ?>
			<?php if ($field->label) : ?>
		<div class="desc-title"><?php echo $field->label; ?></div>
			<?php endif; ?>
		<div class="desc-content"><?php echo $field->display; ?></div>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
<!-- EOF Descriptif block -->

<!-- BOF Gallerie block -->
	<?php if (isset($this->item->positions['galerie'])) : ?>
	<div class="galerie">
		<?php foreach ($this->item->positions['galerie'] as $field) : ?>
			<?php if ($field->label) : ?>
		<div class="desc-title"><?php echo $field->label; ?></div>
			<?php endif; ?>
		<div class="desc-content"><?php echo $field->display; ?></div>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
<!-- EOF Gallerie block -->

	<div class="clear"></div>

	<?php if (isset($this->item->toc)) : ?>
	<!-- BOF TOC -->
	<?php echo $this->item->toc; ?>
	<!-- EOF TOC -->
	<?php endif; ?>

	<div class="clear"></div>

	<?php if (isset($this->item->positions['bottom'])) : ?>
<!-- BOF bottom block -->
	<div class="infoblock <?php echo $this->params->get('bottom_cols', 'two'); ?>cols">
		<ul>
			<?php foreach ($this->item->positions['bottom'] as $field) : ?>
			<li>
				<div>
					<?php if ($field->label) : ?>
					<div class="label field_<?php echo $field->name; ?>"><?php echo $field->label; ?></div>
					<?php endif; ?>
					<div class="value field_<?php echo $field->name; ?>"><?php echo $field->display; ?></div>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
<!-- EOF bottom block -->
	<?php endif; ?>

	<?php if ($this->params->get('comments') && !JRequest::getVar('print')) : ?>
<!-- BOF comments -->
	<div class="comments">
		<?php
		if ($this->params->get('comments') == 1) :
			if (file_exists(JPATH_SITE.DS.'components'.DS.'com_jcomments'.DS.'jcomments.php')) :
				require_once(JPATH_SITE.DS.'components'.DS.'com_jcomments'.DS.'jcomments.php');
				echo JComments::showComments($this->item->id, 'com_flexicontent', $this->escape($this->item->title));
			endif;
		endif;
	
		if ($this->params->get('comments') == 2) :
			if (file_exists(JPATH_SITE.DS.'plugins'.DS.'content'.DS.'jom_comment_bot.php')) :
    			require_once(JPATH_SITE.DS.'plugins'.DS.'content'.DS.'jom_comment_bot.php');
    			echo jomcomment($this->item->id, 'com_flexicontent');
  			endif;
  		endif;
		?>
	</div>
<!-- EOF comments -->
	<?php endif; ?>

</div>