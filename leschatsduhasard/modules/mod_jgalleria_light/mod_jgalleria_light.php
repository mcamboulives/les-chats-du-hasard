<?php
/**
* Author: Tobias Grahn
* Email: info@joohopia.com
* Module: jGalleria
* Version: 1.0.0
**/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$width =  $params->get('width');
$thumbcrop =  $params->get('thumbcrop');
$height =  $params->get('height');
$path = 'modules/mod_jgalleria_light/';
$compurl = JURI::base() . $path;
$pic= "images/stories/".$params->get('folder');
$filelist="";
$descr="tittel";
$theight=$params->get('theight');
$pic=$pic."/";
?>
<script type="text/javascript" src="<?php echo $compurl; ?>scripts/jquery-1.4.3.min.js"></script>
<script type="text/javascript" src="<?php echo $compurl; ?>scripts/galleria.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $compurl; ?>css/galleria.classic.css" />
 <style>
<!--[if lt IE 8]>
.galleria-thumb-nav-left,
.galleria-thumb-nav-right,
.galleria-info-link,
.galleria-info-close,
.galleria-image-nav-left,
.galleria-image-nav-right {
    background-image: url(<?php echo $compurl; ?>css/classic-map.png) !important;
}
<![endif]-->
</style>
	
<style>
.galleria-info-title a{ color:#930; }
.content {
	color:#eee;
	font:14px/1.4 "helvetica neue", arial, sans-serif;
 width:<?php echo $width;?>px;
	margin:20px auto
}
#galleria {
 height:<?php echo $height;?>px;
}

.holder, .holder a, .holder img {
	border:none;
}
* html .galleria-stage { height:100%; }
</style>
<div class="content">
  <div id="galleria">
    <?php
$dirFiles = array();
// opens images folder
if ($handle = opendir($pic)) {
    while (false !== ($file = readdir($handle))) {
        // strips files extensions      
        $crap   = array(".jpg", ".jpeg", ".JPG", ".JPEG", ".png", ".PNG", ".gif", ".GIF", ".bmp", ".BMP", "_", "-");    
        $newstring = '<div class="holder"><a href="http://www.joohopia.com" alt="Joomla Templates and modules"><img src="'.$compurl.'css/joohopia.png" width="112" height="31" alt="Joomla Templates and modules" /></a><br>jGalleria Slideshow module by <a href="http://www.joohopia.com" alt="Joomla Templates and modules">Joohopia<br><br>[this is the free version<br> Limited to 8 images]</a></div>'  ;
        //asort($file, SORT_NUMERIC); - doesnt work :(
       // hides folders, writes out ul of images and thumbnails from two folders
        if ($file != "." && $file != ".." && $file != "index.php" && $file != "index.html" && $file != "thumbs" && $file != "Thumbnails") {
                $dirFiles[] = $file;
        }
    }
    closedir($handle);
}
sort($dirFiles);
$xop=0;
foreach($dirFiles as $file)
{
	$xop++;
	if ($xop==9) {break;}
   echo "<a href='".JURI::base().$pic.$file."'  title='".$newstring."'><img src='".JURI::base().$pic.$file."' title='".$newstring."'  /></a>";
}
?>
  </div>
</div>
<script type="text/javascript">

/*** 
    jGalleria jQuery Slideshow Script
    Original released by Anio
    Modified by joohopia.com 
***/
// Initialize Galleria
   Galleria.loadTheme('<?php echo $compurl; ?>scripts/galleria.classic.js');
$('#galleria').galleria({thumb_crop: '<?php echo $thumbcrop; ?>',image_crop: false});
</script> 