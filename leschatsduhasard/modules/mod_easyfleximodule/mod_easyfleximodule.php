<?php
/*
// "Easy Flexi Module" Module by CCC for Joomla! and Flexicontent 1.5.x
// Copyright (c) 2006 - 2010 ccomca.com All rights reserved.
// Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
// More info at http://www.ccomca.com/communaute/item/314-easy-flexi-module.html
// Designed and developed by the CCC team
// *** Last update: 29/04/2010 ***
*/

// no direct access
defined('_JEXEC') or die('Restricted access');



// Include the syndicate functions only once
require_once (dirname(__FILE__).DS.'helper.php');

// Include a route helper and field information for Flexicontent


$option				= JRequest::getCmd('option');
$view				= JRequest::getCmd('view');

$temp				= JRequest::getString('id');
$temp				= explode(':', $temp);
$id					= $temp[0];


if ($params->get('display_only_item'))
	{
		require_once (JPATH_SITE.DS.'components'.DS.'com_flexicontent'.DS.'helpers'.DS.'route.php');
		require_once (JPATH_SITE.DS.'components'.DS.'com_flexicontent'.DS.'classes'.DS.'flexicontent.helper.php');
		require_once (JPATH_SITE.DS.'components'.DS.'com_flexicontent'.DS.'classes'.DS.'flexicontent.fields.php');
		
		$list = modEasyFlexiModuleHelper::getList($params);
		
		if ($params->get('layout_type') == 'complex')
		{
			require(JModuleHelper::getLayoutPath('mod_easyfleximodule', 'complex'));
		}
		elseif ($params->get('layout_type') == 'custom' && file_exists(JPATH_SITE.DS.'modules'.DS.'mod_easyfleximodule'.DS.'tmpl'.DS.$params->get('custom_name_layout').'.php') )
		{
			require(JModuleHelper::getLayoutPath('mod_easyfleximodule', $params->get('custom_name_layout')));
		}
		else 
		{
			require(JModuleHelper::getLayoutPath('mod_easyfleximodule'));
		}
	}
	else
	{
	if ($option == 'com_flexicontent' && $view == 'items' && $id)
		{
			require_once (JPATH_SITE.DS.'components'.DS.'com_flexicontent'.DS.'helpers'.DS.'route.php');
			require_once (JPATH_SITE.DS.'components'.DS.'com_flexicontent'.DS.'classes'.DS.'flexicontent.helper.php');
			require_once (JPATH_SITE.DS.'components'.DS.'com_flexicontent'.DS.'classes'.DS.'flexicontent.fields.php');
			
			$list = modEasyFlexiModuleHelper::getList($params);
			if ($params->get('layout_type') == 'complex')
			{
				require(JModuleHelper::getLayoutPath('mod_easyfleximodule', 'complex'));
			}
			elseif ($params->get('layout_type') == 'custom' && file_exists(JPATH_SITE.DS.'modules'.DS.'mod_easyfleximodule'.DS.'tmpl'.DS.$params->get('custom_name_layout').'.php') )
			{
				require(JModuleHelper::getLayoutPath('mod_easyfleximodule', $params->get('custom_name_layout')));
			}
			else 
			{
				require(JModuleHelper::getLayoutPath('mod_easyfleximodule'));
			}
		}
		else
		{
			return;
		}
	}


