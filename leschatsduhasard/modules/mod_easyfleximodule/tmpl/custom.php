<?php 
/*
// "Easy Flexi Module" Module by CCC for Joomla! and Flexicontent 1.5.x
// Copyright (c) 2006 - 2010 ccomca.com All rights reserved.
// Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
// More info at http://www.ccomca.com/communaute/item/314-easy-flexi-module.html
// Designed and developed by the CCC team
// *** Last update: 02/05/2010 ***
*/

/*
// This file is an example.
// For display a custom field, add this : $item->myfield

*/

// no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<?php foreach ($list as $item) : ?>

<div class="item">
<a href="<?php echo $item->link; ?>">
  <div class="description">
	<?php //print_r ($item); ?>
  	
	<?php if (isset($item->src)){
		if (strlen($item->src)>0) echo "<img alt=".$item->text." src=".$item->thumb.">"; 
	}?>
	
	<div class="title">
		<p style="text-align: center; padding-top:3px; margin-bottom: 0px;"><?php echo mb_strtoupper ($item->text); ?></p>
	</div>
  </div>
  </a>
</div>
<?php endforeach; ?>