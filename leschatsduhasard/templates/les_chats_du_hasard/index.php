<?php
/**
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/les_chats_du_hasard/css/template.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/les_chats_du_hasard/css/<?php echo $this->params->get('colorVariation'); ?>.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/les_chats_du_hasard/css/<?php echo $this->params->get('backgroundVariation'); ?>_bg.css" type="text/css" />
<!--[if lte IE 6]>
<link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/ieonly.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if lte IE 8]>
<link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/ie8only.css" rel="stylesheet" type="text/css" />
<![endif]-->
<?php if($this->direction == 'rtl') : ?>
	<link href="<?php echo $this->baseurl ?>/templates/les_chats_du_hasard/css/template_rtl.css" rel="stylesheet" type="text/css" />
<?php endif; ?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="http://apis.google.com/js/plusone.js" type="text/javascript">{lang: 'fr'}</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-27467703-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body id="page_bg" class="color_<?php echo $this->params->get('colorVariation'); ?> bg_<?php echo $this->params->get('backgroundVariation'); ?> width_<?php echo $this->params->get('widthStyle'); ?>">
<a name="up" id="up"></a>
<center>
<div id="page_fond" class="center" align="center">
	
    <!-- wrapper -->
    <div id="wrapper">    
    	
		<div id="wrapper_r">
<!--[if lt IE 6]>
  <div>
	<div style="border: 1px solid rgb(247, 148, 29); background: none repeat scroll 0% 0% rgb(254, 239, 218); text-align: center; clear: both; height: 75px; position: relative; width: 100%;">
		<div style="margin: 0pt auto; padding: 0pt; overflow: hidden; color: black; text-align: center; width: 100%;">
			<div style="float: left; font-family: Arial,sans-serif; width: 100%;">
				<div style="font-size: 14px; font-weight: bold; margin-top: 12px;">Vous utilisez un navigateur dépassé depuis près de 8 ans!</div>
				<div style="font-size: 12px; margin-top: 6px; line-height: 12px;">Pour une meilleure expérience web, prenez le temps de mettre votre navigateur à jour.</div>
			</div>
		</div>
	</div>
</div>
  <![endif]-->	
            <!-- header -->
            <div id="header">
				<div id="header_l">
					<div id="header_r">
<jdoc:include type="modules" name="top" />
                    	<a href="<?php echo $this->baseurl ?>" title="Retour à la page d'accueil">
						<span id="logo"></span>
						<span id="header_title"></span>
						
                        </a>
					</div>
				</div>
			</div>

	    <!-- tabarea -->
            <div id="tabarea">
				<div class="chat-search"></div>
				<div id="tabarea_l">
					<div id="tabarea_r">
						<div id="tabmenu">
						<table cellpadding="0" cellspacing="0" class="pill">
							<tr>
								<td class="pill_l">&nbsp;</td>
								<td class="pill_m">
								<div id="pillmenu">
									<jdoc:include type="modules" name="user3" />
								</div>
								</td>
								<td class="pill_r">&nbsp;</td>
							</tr>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="clr"></div>

			<!--  -->
            <div id="whitebox">
				<div id="whitebox_t">
					<div id="whitebox_tl">
						<div id="whitebox_tr"></div>
					</div>
				</div>

				<div id="whitebox_m">
                	
                    <!-- search : user4 -->
                    <div id="search">
                        <jdoc:include type="modules" name="user4" />
                    </div>
                
					<div id="area">
						
                        <jdoc:include type="message" />

						<?php if($this->countModules('left')) : ?><div id="leftcolumn">
							
							<jdoc:include type="modules" name="left" style="rounded" />                            
							                    	
						</div>
                        <?php endif; ?>    
                       

						<?php if($this->countModules('left')) : ?>
						<div id="maincolumn">
						<?php else: ?>
						<div id="maincolumn_full">
						<?php endif; ?>
                        

                            <!-- pathway : breadcrumb -->
                            <div id="pathway">
                                <jdoc:include type="modules" name="breadcrumb" />
                            </div>

                        
							<?php if($this->countModules('user1 or user2')) : ?>
								<table class="nopad user1user2">
									<tr valign="top">
										<?php if($this->countModules('user1')) : ?>
											<td>
												<jdoc:include type="modules" name="user1" style="xhtml" />
											</td>
										<?php endif; ?>
										<?php if($this->countModules('user1 and user2')) : ?>
											<td class="greyline">&nbsp;</td>
										<?php endif; ?>
										<?php if($this->countModules('user2')) : ?>
											<td>
												<jdoc:include type="modules" name="user2" style="xhtml" />
											</td>
										<?php endif; ?>
									</tr>
								</table>

								<div id="maindivider"></div>
							<?php endif; ?>

							<table class="nopad">
								<tr valign="top">
									<td>
										<jdoc:include type="component" />
										<jdoc:include type = "message" /> 
										<jdoc:include type="modules" name="footer_content" style="xhtml"/>
									</td>
									<?php if($this->countModules('right') and JRequest::getCmd('layout') != 'form') : ?>
										<td class="greyline">&nbsp;</td>
										<td width="170">
											<jdoc:include type="modules" name="right" style="xhtml"/>
										</td>
									<?php endif; ?>
								</tr>
							</table>

						</div>
						<div class="clr"></div>
					</div>
					<div class="clr"></div>
				</div>

				<div id="whitebox_b">
					<div id="whitebox_bl">
						<div id="whitebox_br"></div>
					</div>
				</div>
			</div>

			<div id="footerspacer"></div>
            
            <!-- footer -->
            <div id="footer">
                <div id="footer_l">
                    <div id="footer_r">
                    	<div id="footer_deco"></div>                        
                        <div id="footer_footer">
                            <jdoc:include type="modules" name="footer" />
                        </div>
                        <div class="clr"></div>
                        <div id="syndicate">
                            <jdoc:include type="modules" name="syndicate" />
                        </div>
                        <div id="power_by">
                            <?php echo JText::_('Powered by') ?> <a href="http://www.joomla.org">Joomla!</a>.
                            <?php echo JText::_('Valid') ?> <a href="http://validator.w3.org/check/referer">XHTML</a> <?php echo JText::_('and') ?> <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>.
                        </div>
                    </div>
                </div>
            </div>
        
		</div>		
        <!-- fin wrapper_r -->
        
	</div>
    <!-- fin wrapper -->
    
</div>
<jdoc:include type="modules" name="debug" />

</center>
</body>
</html>