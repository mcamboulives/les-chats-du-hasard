<?php
/*
// "Easy Flexi Module" Module by CCC for Joomla! and Flexicontent 1.5.x
// Copyright (c) 2006 - 2010 ccomca.com All rights reserved.
// Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
// More info at http://www.ccomca.com/communaute/item/314-easy-flexi-module.html
// Designed and developed by the CCC team
// *** Last update: 29/04/2010 ***
*/

//-- No direct access
defined('_JEXEC') or die();

class JElementEasyfleximoduleHead extends JElement
{
	var	$_name = 'Easyfleximodule';

	function fetchTooltip($label, $description, &$node, $control_name, $name) {
		return '&nbsp;';
	}

	function fetchElement($name, $value, &$node, $control_name)
	{
		
		if ($node->attributes( 'icon' ) != "") $icon = "<img src=\"".JURI::root(true)."/modules/mod_easyfleximodule/assets/images/icons/".$node->attributes( 'icon' )."\" alt =\"" . JText::_($value) . "\" align=\"absmiddle\" vspace=\"5\" />";
		
		if ($value) {
			return '<p style="background: #CCE6FF;color: #0069CC;padding:5px">'.$icon.' <strong>' . JText::_($value) . '</strong></p>';
		} else {
			return '<hr />';
		}
	}
}
?>