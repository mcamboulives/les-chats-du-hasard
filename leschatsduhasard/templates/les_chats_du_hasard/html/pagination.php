<?php
/**
 * @version		$Id: pagination.php 14401 2010-01-26 14:10:00Z louis $
 * @package		Joomla
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * This is a file to add template specific chrome to pagination rendering.
 *
 * pagination_list_footer
 * 	Input variable $list is an array with offsets:
 * 		$list[limit]		: int
 * 		$list[limitstart]	: int
 * 		$list[total]		: int
 * 		$list[limitfield]	: string
 * 		$list[pagescounter]	: string
 * 		$list[pageslinks]	: string
 *
 * pagination_list_render
 * 	Input variable $list is an array with offsets:
 * 		$list[all]
 * 			[data]		: string
 * 			[active]	: boolean
 * 		$list[start]
 * 			[data]		: string
 * 			[active]	: boolean
 * 		$list[previous]
 * 			[data]		: string
 * 			[active]	: boolean
 * 		$list[next]
 * 			[data]		: string
 * 			[active]	: boolean
 * 		$list[end]
 * 			[data]		: string
 * 			[active]	: boolean
 * 		$list[pages]
 * 			[{PAGE}][data]		: string
 * 			[{PAGE}][active]	: boolean
 *
 * pagination_item_active
 * 	Input variable $item is an object with fields:
 * 		$item->base	: integer
 * 		$item->link	: string
 * 		$item->text	: string
 *
 * pagination_item_inactive
 * 	Input variable $item is an object with fields:
 * 		$item->base	: integer
 * 		$item->link	: string
 * 		$item->text	: string
 *
 * This gives template designers ultimate control over how pagination is rendered.
 *
 * NOTE: If you override pagination_item_active OR pagination_item_inactive you MUST override them both
 */

function pagination_list_footer($list)
{
	$html = "<div class=\"list-footer\">\n";

	$html .= "\n<div class=\"limit\">".JText::_('Display Num').$list['limitfield']."</div>";
	$html .= $list['pageslinks'];
	$html .= "\n<div class=\"counter\">".$list['pagescounddter']."</div>";

	$html .= "\n<input type=\"hidden\" name=\"limitstart\" value=\"".$list['limitstart']."\" />";
	$html .= "\n</div>";

	return $html;
}

function pagination_list_render($list)
{
	// Initialize variables
	$html = "<div class=\"pagination\">";
	
	//echo "<pre>"; var_dump($list['pages']); echo "</pre>";
	$nb_pages = count($list['pages']);
	$page_active = 0;
	$i=0;
	
	foreach( $list['pages'] as $num_page=>$page )
	{
		$i++; //i�me page
		
		//page actie ?
		if(!($page['active'])) {
			$class = ' class="active"';	$page_active = 1;
		} else {
			$class = ' class="no-active"'; 	$page_active = 0;
		}
		
		//affichage des pages next et fin que si page courante est la premi�re
		if($i == 1 && !($page_active)) {
			$html .= '<span class="start">'.$list['start']['data'].'</span>';
			$html .= '<span class="previous">'.$list['previous']['data'].'</span>';
		}
	
		//numero de page avec lien ou non
		$html .= '<span'.$class.'>';
		$html .= $page['data'];		
		$html .= '</span>';
		
		//affichage des pages next et fin que si page courante est la derni�re
		if($i == $nb_pages && !($page_active)) {
			$html .= '<span class="next">'.$list['next']['data'].'</span>';
			$html .= '<span class="end">'.$list['end']['data'].'</span>';
		}
	}
	
	$html .= "</div>";
	return $html;
}

function pagination_item_active(&$item) {
	$class = '';
	if(is_string($item->text)) {
		$class = " class='hidden'";
	}
	
	return "<a href=\"".$item->link."\" title=\"".$item->text."\"><span".$class.">".$item->text."</span></a>";
}

function pagination_item_inactive(&$item) {
	return "<span>".$item->text."</span>";
}
?>