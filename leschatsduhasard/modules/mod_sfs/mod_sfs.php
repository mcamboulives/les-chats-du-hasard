<?php
/*------------------------------------------------------------------------
# mod_sfs - Simple Flickr Slideshow for Joomla 1.5 v1.0.2
# ------------------------------------------------------------------------
# author    GraphicAholic
# copyright Copyright (C) 2011 GraphicAholic.com. All Rights Reserved.
# license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.graphicaholic.com
-------------------------------------------------------------------------*/

// No direct access
defined('_JEXEC') or die('Restricted access');
$LiveSite 	= JURI::base();
$slideWidth 	= $params->get('slideWidth', '');
$slideHeight 	= $params->get('slideHeight', '');
$topMargin 	= $params->get('topMargin', '');
$bottomMargin 	= $params->get('bottomMargin', '');
$interval 	= $params->get('interval', '');
$autoStart = $params->get('autoStart','');
if($autoStart == "0") $autoStart = "true";
if($autoStart == "1") $autoStart = "false";
$photoSet 	= $params->get('photoSet', '');
$hide_buttons = $params->get('hide_buttons','');
if($hide_buttons == "0") $hide_buttons = "false";
if($hide_buttons == "1") $hide_buttons = "true";
$moduleId = $module->id;
//append javascript along with css files
$document =& JFactory::getDocument();		
$document->addScript(JURI::root().'modules/mod_sfs/js/flickrshow-7.2.js');
?>
<div class="example" id="cesc<?php echo $moduleId; ?>" style="height:<?php echo $slideHeight; ?>px;width:<?php echo $slideWidth; ?>px; margin-top: <?php echo $topMargin; ?>px;margin-bottom:<?php echo $bottomMargin; ?>px;">
<p>Loading script and Flickr images</p>
</div>
<script>
var cesc<?php echo $moduleId; ?> = new flickrshow('cesc<?php echo $moduleId; ?>', {
'set':'<?php echo $photoSet; ?>',
autoplay:<?php echo $autoStart; ?>,
interval:<?php echo $interval; ?>,
hide_buttons:<?php echo $hide_buttons; ?>
});
</script>