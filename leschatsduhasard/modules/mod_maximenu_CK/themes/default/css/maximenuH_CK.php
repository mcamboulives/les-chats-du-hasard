<?php
header('content-type: text/css');
$id = $_GET['monid'];
?>

.clr {clear:both;}

/**
** global styles
**/

/* container style */
div#<?php echo $id; ?> ul.maximenuCK {
    /*background :  url(../images/fond_bg.png) top left repeat-x #571D2F;*/
    background-color : #571D2F;
    height : 41px;
    padding : 0;
    margin : 0;
    color: #fff;
}

div#<?php echo $id; ?> ul.maximenuCK li.maximenuCK {
    background : none;
    list-style : none;
    border : none;
}


/* link image style */
div#<?php echo $id; ?> ul.maximenuCK li.maximenuCK>a img {
    margin : 3px;
    border : none;
}

/* img style without link (in separator) */
div#<?php echo $id; ?> ul.maximenuCK li.maximenuCK img {
    border : none;
}

div#<?php echo $id; ?> ul.maximenuCK li a.maximenuCK,
div#<?php echo $id; ?> ul.maximenuCK li span.separator {
    text-decoration : none;
    text-indent : 2px;
    height : 29px;
    outline : none;
    background : none;
    border : none;
    padding : 0;
    cursor : pointer;
    color : #fff;
}

/* separator item */
div#<?php echo $id; ?> ul.maximenuCK li span.separator {

}

/**
** active items
**/

/* current item title and description */
div#<?php echo $id; ?> ul.maximenuCK li.current>a span {
    color : #fff;
}

/* current item title when mouseover */
div#<?php echo $id; ?> ul.maximenuCK li.current>a:hover span.titreCK {
    font-weight: bold;
}

/* current item description when mouseover */
div#<?php echo $id; ?> ul.maximenuCK li.current>a:hover span.descCK {

}

/* active parent title */
div#<?php echo $id; ?> ul.maximenuCK li.active>a span.titreCK {
    color : #fff;
    font-weight: bold;
}

/* active parent description */
div#<?php echo $id; ?> ul.maximenuCK li.active>a span.descCK {

}

/**
** first level items
**/

div#<?php echo $id; ?> ul.maximenuCK li.level0 {
    /* background : url(../images/puce_menu.png) left 10px no-repeat;*/
    background: url(../images/puce_menu.png) no-repeat 0 0px transparent;
    margin: 11px 0 0px 15px;
    padding: 1px 0px 0px 26px;
}

/* first level item title */
div#<?php echo $id; ?> ul.maximenuCK li.level0>a span.titreCK,
div#<?php echo $id; ?> ul.maximenuCK li.level0>span.separator span.titreCK {
    color : #fff;
    font-weight: bold;
}

/* first level item description */
div#<?php echo $id; ?> ul.maximenuCK li.level0>a span.descCK {
    color : #fff;
}

/* first level item link */
div#<?php echo $id; ?> ul.maximenuCK li.parent.level0>a,
div#<?php echo $id; ?> ul.maximenuCK li.parent.level0>span {
    background : url(../images/maxi_arrow0.png) bottom right no-repeat;
}

/* parent style level 0 */
div#<?php echo $id; ?> ul.maximenuCK li.parent.level0 li.parent {
    background : url(../images/maxi_arrow1.png) center right no-repeat;
}

/**
** items title and descriptions
**/

/* item title */
div#<?php echo $id; ?> span.titreCK {
    color : #fff;
    /*display : block;*/
    text-transform : none;
    font-size : 14px;
    line-height : 18px;
    text-decoration : none;
    min-height : 17px;
    float : none !important;
    float : left;
    font-weight: bold;
}

/* item description */
div#<?php echo $id; ?> span.descCK {
    color : #c0c0c0;
    display : block;
    text-transform : none;
    font-size : 10px;
    text-decoration : none;
    height : 12px;
    line-height : 12px;
    float : none !important;
    float : left;
}

/* item title when mouseover */
div#<?php echo $id; ?> ul.maximenuCK2 li.maximenuCK:hover {
    background-color: #d08484;
}
div#<?php echo $id; ?> ul.maximenuCK  a:hover span.titreCK {
    color : #fff;
    font-weight: bold;
}

/**
** child items
**/

/* child item title */
div#<?php echo $id; ?> ul.maximenuCK2  a.maximenuCK {
    width : 160px;
}

div#<?php echo $id; ?> ul.maximenuCK2 li a.maximenuCK {
    text-decoration : none;
    /*width : 180px;*/
    margin : 0 auto;
    padding : 0;
}

div#<?php echo $id; ?> ul.maximenuCK2 li span.separator {
    text-decoration : none;
    border-bottom : 1px solid #fff;
    /*width : 180px;*/
    margin : 0 auto;
    padding : 0;
}

/* child item block */
div#<?php echo $id; ?> ul.maximenuCK ul.maximenuCK2 {
    background : #996565;
    margin : 0;
    padding : 0;
    border : none;
    width : 180px; /* important for Chrome and Safari compatibility */
    position: static;
}

div#<?php echo $id; ?> ul.maximenuCK2 li.maximenuCK {
    width : 170px;
    padding : 0 0 0 10px;
    border-bottom : 1px solid #fff;
    margin : 0;
    background : none;
    list-style : disc inside url("../images/puce-patoune.png");
    line-height: 30px;
}

div#<?php echo $id; ?> ul.maximenuCK2 li.maximenuCK a{
    display: inline;
    text-decoration: none;
}

div#<?php echo $id; ?> ul.maximenuCK2 li.maximenuCK span.titreCK{
    font-size: 10px;
}



/* child item container  */
div#<?php echo $id; ?> ul.maximenuCK li div.floatCK {
    background : #996565;
    border-left : 1px solid #fff;
    border-bottom : 1px solid #fff;
    border-right : 1px solid #fff;
}

/**
** module style
**/

div#<?php echo $id; ?> div.maximenuCK_mod {
    width : 170px;
    padding : 0;
    overflow : hidden;
    color : #ddd;
    white-space : normal;
}

div#<?php echo $id; ?> div.maximenuCK_mod div.moduletable {
    border : none;
    background : none;
}

div#<?php echo $id; ?> div.maximenuCK_mod  fieldset{
    width : 160px;
    padding : 0;
    margin : 0 auto;
    overflow : hidden;
    background : #996565;
    border : none;
}

div#<?php echo $id; ?> ul.maximenuCK2 div.maximenuCK_mod a {
    border : none;
    margin : 0;
    padding : 0;
    display : inline;
    background : #996565;
    color : #fff;
    font-weight : normal;
}

div#<?php echo $id; ?> ul.maximenuCK2 div.maximenuCK_mod a:hover {
    color : #FFF;
}

/* module title */
div#<?php echo $id; ?> ul.maximenuCK div.maximenuCK_mod h3 {
    font-size : 14px;
    width : 170px;
    color : #aaa;
    font-size : 14px;
    font-weight : normal;
    background : #444;
    margin : 5px 0 0 0;
    padding : 3px 0 3px 0;
}

div#<?php echo $id; ?> ul.maximenuCK2 div.maximenuCK_mod ul {
    margin : 0;
    padding : 0;
    width : 170px;
    background : none;
    border : none;
    text-align : left;
}

div#<?php echo $id; ?> ul.maximenuCK2 div.maximenuCK_mod li {
    margin : 0 0 0 15px;
    padding : 0;
    width : 155px;
    background : none;
    border : none;
    text-align : left;
    font-size : 11px;
    float : none;
    display : block;
    line-height : 20px;
    white-space : normal;
}

/* login module */
div#<?php echo $id; ?> ul.maximenuCK2 div.maximenuCK_mod #form-login ul {
    left : 0;
    margin : 0;
    padding : 0;
    width : 170px;
}

div#<?php echo $id; ?> ul.maximenuCK2 div.maximenuCK_mod #form-login ul li {
    margin : 2px 0;
    padding : 0 5px;
    height : 20px;
    background : #996565;
}


/**
** columns width & child position
**/

/* child blocks position (from level2 to n) */
div#<?php echo $id; ?> ul.maximenuCK li.maximenuCK div.floatCK div.floatCK {
    margin : -30px 0 0 180px;
}

/* margin for overflown elements that rolls to the left */
div#<?php echo $id; ?> ul.maximenuCK li.maximenuCK div.floatCK div.floatCK.fixRight  {
    margin-right : 180px;
}

/* default width */
div#<?php echo $id; ?> ul.maximenuCK li div.floatCK {
    width : 180px;
}

/* 2 cols width */
div#<?php echo $id; ?> ul.maximenuCK li div.cols2 {
    width : 360px;
}

div#<?php echo $id; ?> ul.maximenuCK li div.cols2>div.maximenuCK2 {
    width : 50%;
}

/* 3 cols width */
div#<?php echo $id; ?> ul.maximenuCK li div.cols3 {
    width : 540px;
}

div#<?php echo $id; ?> ul.maximenuCK li div.cols3>div.maximenuCK2 {
    width : 33%;
}

/* 4 cols width */
div#<?php echo $id; ?> ul.maximenuCK li div.cols4 {
    width : 720px;
}

div#<?php echo $id; ?> ul.maximenuCK li div.cols4>div.maximenuCK2 {
    width : 25%;
}



/**
** fancy parameters
**/

div#<?php echo $id; ?> .maxiFancybackground {
    list-style : none;
}

div#<?php echo $id; ?> .maxiFancybackground .maxiFancycenter {
    /*background: url('../images/fancy_bg.png') repeat-x top left;*/
    background-color: #d08484;
    border-radius: 10px;
    height : 23px;
    margin: 10px 5px;
    padding: 0px 25px 0px 25px;
}

div#<?php echo $id; ?> .maxiFancybackground .maxiFancyleft {

}

div#<?php echo $id; ?> .maxiFancybackground .maxiFancyright {

}

/**
** rounded style
**/

/* global container */
div#<?php echo $id; ?> div.maxiRoundedleft {

}

div#<?php echo $id; ?> div.maxiRoundedcenter {

}

div#<?php echo $id; ?> div.maxiRoundedright {

}

/* child container */
div#<?php echo $id; ?> div.maxidrop-top {

}

div#<?php echo $id; ?> div.maxidrop-main {

}

div#<?php echo $id; ?> div.maxidrop-bottom {

}


/* bouton to close on click */
div#<?php echo $id; ?> span.maxiclose {
    color: #fff;
}