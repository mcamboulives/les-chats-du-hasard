<?php
/*
// "Easy Flexi Module" Module by CCC for Joomla! and Flexicontent 1.5.x
// Copyright (c) 2006 - 2010 ccomca.com All rights reserved.
// Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
// More info at http://www.ccomca.com/communaute/item/314-easy-flexi-module.html
// Designed and developed by the CCC team
// *** Last update: 03/05/2010 ***
//Inspired by RockNavMenu (RocketWerx)
*/


// Check to ensure this file is within the rest of the framework
defined('JPATH_BASE') or die();

/**
 * Renders a file list from a directory in the current templates directory
 */

class JElementExtendedFieldNameValue extends JElement
{
	/**
	* Element name
	*
	* @access	protected
	* @var		string
	*/
	var	$_name = 'ExtendedFieldNameValue';
	
	function fetchElement($name, $value, &$node, $control_name)
	{
		$doc =& JFactory::getDocument();
		$all_params = $this->_parent;
		$count = 0;
		
	
		
		$namevalues = array(); 
		//get all current values
		while (true) { 
			$count++;
			$current_name = $all_params->get($name.'_name_'.$count, '');
			if ($current_name=='') {
				break;
			}
			$current_value = $all_params->get($name.'_value_'.$count, '');
			$namevalues[$count]=array('name'=>$current_name);
		}
	
//		if (count($namevalues) == 0) {
//			$namevalues[1]=array('name'=>'','value'=>'');
//		}
		
		$html='';
		$html = '<button id="add_more">' . JText::_('Add') . '</button><br /><br />' .
				'<div id="more-fields">';
		for($i=1; $i <= count($namevalues); $i++){
			$html .= '<div id="'. $name. '_' .$i. '" class="easyfleximodule-extendedfield" style="margin: 5px 0pt;">' 
				.'<input type="text" name="' . $control_name.'['.$name. '_name_' .$i. ']" value="'.$namevalues[$i]['name'].'" size="10 " />'
				.'</div>';
		}
		$html .= '</div>';
		
		$plugin_js_path = JURI::root(true).'/modules/mod_easyfleximodule/assets/js';
    	$doc->addScript($plugin_js_path."/extendedfield.js");
		$doc->addScriptDeclaration("var extendedfieldSettings = {'basename': '" . $name.   "', 'params': '" . $control_name. "'};");
	
		return $html;
	}
}
