<?php
/**
* @version		$Id:editdisposable.php  2009-22-02$
* @package		Joomla.Framework
* @subpackage	Parameter
* @copyright		Copyright (C) 2009 David Barrett. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*/

// Check to ensure this file is within the rest of the framework
defined('JPATH_BASE') or die();

/**
 * Renders a multi-select category list (not drop-down)
 *
 * @package 	Joomla.Framework
 * @subpackage		Parameter
 * @since		1.5
 */

class JElementEditdisposable extends JElement
{
	/**
	* Element name
	*
	* @access	protected
	* @var		string
	*/
	var	$_name = 'Editdisposable';

	function fetchElement($name, $value, &$xmlElement, $control_name)
	{
		$class		= $xmlElement->attributes('class');
		if (!$class) {
			$class = "";
		}
		
		$arr_value=split(',', $value);

		// Javascript helper functions to populate the hidden field
		global $JElementEditdisposableJSWritten;
		if (!$JElementEditdisposableJSWritten) {
			$js='<script language="javascript" type="text/javascript">
				function inArray( arr, search ) {
				  var len = arr.length;
				  for ( var x = 0 ; x <= len ; x++ ) {
					 if ( arr[x] == search ) return true;
				  }
				  return false;
				}

				function compareOptionText(a,b) {
				  return a.text!=b.text ? a.text<b.text ? -1 : 1 : 0;
				}
				
				function sortOptions(list) {
				  var items = list.options.length;
				  var tmpArray = new Array(items);
				  var i=0;
				  for ( i=0; i<items; i++ )
					 { tmpArray[i]=new Option(list.options[i].text,list.options[i].value);}
				  tmpArray.sort(compareOptionText);
				  for ( i=0; i<items; i++ )
					 { list.options[i] = new Option(tmpArray[i].text,tmpArray[i].value); }
				}

				function addItemToList(list, item) {
				  try {
					 list.options.add(new Option(item, item),null);
				  } catch(e) {
					 list.options.add(new Option(item, item));}
				}
				
				function removeListDuplicates(list) {
				  var n = list.options.length;
				  var i = 0;
				  var arr_Items=new Array(n);
				  var item = "";
				  for ( i=0; i<n; i++ ) {
				    item=list.options[i].value.toString();
					 if (inArray(arr_Items, item)) {
						list.remove(i);
						i--;
						n--;
					 } else {
				      arr_Items[i]=list.options[i].value.toString();
					 }
				  }
				}
				
				function addToList(controlname, name) {
				  var t = document.getElementById(controlname + name + "text");
				  var l = document.getElementById(controlname + name + "list");
				  var h = document.getElementById(controlname + "[" + name + "]");
				  var item = t.value.toString();
				  var multiadd = item.split(",");
				  item = "";
				  for ( i=0; i<multiadd.length; i++ ) {
					 item = multiadd[i].replace(/^\s+|\s+$/g, "");
				    addItemToList(l, item);
				  }
				  removeListDuplicates(l);
				  sortOptions(l);
				  var s = "";
				  var n = l.options.length;
				  for (i = 0; i < n; i++) {
					 if (s!="") {s=s+",";}
					 s=s+l.options[i].value.toString();
				  }
				  h.value = s;
				  t.value = "";
				}
				
				function deleteFromList(controlname,name) {
				  var l = document.getElementById(controlname + name + "list");
				  var h = document.getElementById(controlname + "[" + name + "]");
				  var i = 0;
				  var n = l.options.length;
				  var s = "";
				  for (i = 0; i < n; i++) {
					 if (s!="") {s=s+",";}
					 if (l.options[i].selected) {
						l.remove(i);
						i=i-1;
						n=n-1;
					 } else {
						s=s+l.options[i].value.toString();
					 }
				  }
				  h.value=s;
				}
				</script>';
			$JElementEditdisposableJSWritten=true;
			$html = $js;
		} else {
			$html='';
		}
		
		$arr_selections=split(',', $value);
		
		// Hidden type to hold the data
		$html .= '<input type="hidden" name="' . $control_name . '[' . $name . ']"' . 
					'" id="' . $control_name . '[' . $name . ']"' .
					' class="' .$class .'"' .
					' value="' . $value . '">';
					
		// Write the list
		$html .= '<select name="' . $control_name . $name . 'list"' . 
					'" id="' . $control_name .  $name . 'list"' . 
					' size="10" multiple="extended">';		
		foreach ($arr_selections as $option) {
			if ($option!="") {
				$html .= '<option value="' . $option . '">' .
						$option . '</option>';
			}
		}		
		$html .= '</select><br />';

		// Text type for adding an item to the list
		$html .= '<textarea name="' . $control_name . $name . 'text"' .
					'" id="' . $control_name . $name . 'text" cols="30" rows="1"></textarea>';
					
		// Add button
		$html .= '<input type="button" name="' . $control_name . $name . 'addbutton"' .
					'" id="' . $control_name . $name . 'addbutton"' . 
					' value="Add"' .
					' onClick="javascript: addToList(\'' . $control_name . '\', \'' . $name . '\')">';
		
		// Delete button
		$html .= '<input type="button" name="' . $control_name . $name . 'deletebutton"' .
					'" id="' . $control_name . $name . 'deletebutton"' . 
					' value="Delete Selected"' .
					' onClick="javascript: deleteFromList(\'' . $control_name . '\', \'' . $name . '\')">';
			
		return $html;
	}
}
