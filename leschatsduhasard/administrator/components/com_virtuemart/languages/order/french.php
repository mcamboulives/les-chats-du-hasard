<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
* @version $Id: french.php 1501 2010-12-22 10:45:28Z jollant $
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
* @translator soeren
* @french translations by Val�rie Isaksen aka Alatak and Patrick Jollant aka PATSXM971
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'ISO-8859-15',
	'PHPSHOP_ORDER_PRINT_PAYMENT_LOG_LBL' => 'Historique des paiements',
	'PHPSHOP_ORDER_PRINT_SHIPPING_PRICE_LBL' => 'Prix livraison',
	'PHPSHOP_ORDER_STATUS_LIST_CODE' => 'Code de l\'�tat de la commande',
	'PHPSHOP_ORDER_STATUS_LIST_NAME' => 'Nom de l\'�tat de la commande',
	'PHPSHOP_ORDER_STATUS_FORM_LBL' => 'Etat de la commande',
	'PHPSHOP_ORDER_STATUS_FORM_CODE' => 'Code de l\'�tat de la commande',
	'PHPSHOP_ORDER_STATUS_FORM_NAME' => 'Nom de l\'�tat de la commande',
	'PHPSHOP_ORDER_STATUS_FORM_LIST_ORDER' => 'Ordre d\'affichage',
	'PHPSHOP_COMMENT' => 'Commentaire',
	'PHPSHOP_ORDER_LIST_NOTIFY' => 'Avertir le client ?',
	'PHPSHOP_ORDER_LIST_NOTIFY_ERR' => 'Veuillez modifier le statut de la commande',
	'PHPSHOP_ORDER_HISTORY_INCLUDE_COMMENT' => 'Inclure ce commentaire ?',
	'PHPSHOP_ORDER_HISTORY_DATE_ADDED' => 'Date ajout�e',
	'PHPSHOP_ORDER_HISTORY_CUSTOMER_NOTIFIED' => 'Avertir client ?',
	'PHPSHOP_ORDER_STATUS_CHANGE' => 'Etat de commande',
	'PHPSHOP_ORDER_LIST_PRINT_LABEL' => 'Imprimer le libell� ',
	'PHPSHOP_ORDER_LIST_VOID_LABEL' => 'Libell� manquant',
	'PHPSHOP_ORDER_LIST_TRACK' => 'Tracking',
	'VM_DOWNLOAD_STATS' => 'T�l�charger les stats',
	'VM_DOWNLOAD_NOTHING_LEFT' => 'Aucun t�l�chargement en attente',
	'VM_DOWNLOAD_REENABLE' => 'R�-activer le t�l�chargement',
	'VM_DOWNLOAD_REMAINING_DOWNLOADS' => 'T�l�chargement en attente',
	'VM_DOWNLOAD_RESEND_ID' => 'Renvoyer l\'ID de  t�l�chargement',
	'VM_EXPIRY' => 'Ech�ance',
	'VM_UPDATE_STATUS' => 'Mise � jour de l\'�tat',
	'VM_ORDER_LABEL_ORDERID_NOTVALID' => 'Merci de fournir une ID de commande valide et num�rique et non pas "(order_id )"',
	'VM_ORDER_LABEL_NOTFOUND' => 'Commande introuvable dans la base de donn�es des lib�ll�s d\'exp�dition.',
	'VM_ORDER_LABEL_NEVERGENERATED' => 'Le libell� n\'a pas encore �t� g�n�r�',
	'VM_ORDER_LABEL_CLASSCANNOT' => 'Classe ship_class () ne peut pas avoir le nom des images, pourquoi sommes-nous ici?',
	'VM_ORDER_LABEL_SHIPPINGLABEL_LBL' => 'Libell� d\'exp�dition',
	'VM_ORDER_LABEL_SIGNATURENEVER' => 'Signature n\'a jamais �t� retrouv�',
	'VM_ORDER_LABEL_TRACK_TITLE' => 'Tracking',
	'VM_ORDER_LABEL_VOID_TITLE' => 'Libell� manquant',
	'VM_ORDER_LABEL_VOIDED_MSG' => 'Le libell� pour la lettre de transport {tracking_number} a �t� annul�.',
	'VM_ORDER_PRINT_PO_IPADDRESS' => 'Adresse Ip',
	'VM_ORDER_STATUS_ICON_ALT' => 'Ic�ne status',
	'VM_ORDER_PAYMENT_CCV_CODE' => 'Code CVV',
	'VM_ORDER_NOTFOUND' => 'Aucune commande n\'a �t� trouv�e. Elle a peut �tre �t� effac�e.',
	'PHPSHOP_ORDER_EDIT_ACTIONS' => 'Actions',
	'PHPSHOP_ORDER_EDIT' => 'Modifier le contenu de la commande',
	'PHPSHOP_ORDER_EDIT_ADD' => 'Ajouter',
	'PHPSHOP_ORDER_EDIT_ADD_PRODUCT' => 'Ajouter produit',
	'PHPSHOP_ORDER_EDIT_EDIT_ORDER' => 'Modifier la commande',
	'PHPSHOP_ORDER_EDIT_ERROR_QUANTITY_MUST_BE_HIGHER_THAN_0' => 'La quantit� doit �tre sup�rieure � 0.',
	'PHPSHOP_ORDER_EDIT_PRODUCT_ADDED' => 'Le produit a �t� ajout� � la commande',
	'PHPSHOP_ORDER_EDIT_PRODUCT_DELETED' => 'Le produit a �t� supprim� de cette commande',
	'PHPSHOP_ORDER_EDIT_QUANTITY_UPDATED' => 'La quantit� a �t� mise � jour',
	'PHPSHOP_ORDER_EDIT_RETURN_PARENTS' => 'Retour au produit parent',
	'PHPSHOP_ORDER_EDIT_CHOOSE_PRODUCT' => 'Selectionner un produit',
	'PHPSHOP_ORDER_CHANGE_UPD_BILL' => 'Modifier l\'adresse de facturation',
	'PHPSHOP_ORDER_CHANGE_UPD_SHIP' => 'Modifier l\'adresse de livraison',
	'PHPSHOP_ORDER_EDIT_SOMETHING_HAS_CHANGED' => 'a chang�',
	'PHPSHOP_ORDER_EDIT_CHOOSE_PRODUCT_BY_SKU' => 'S�lectionnez la r�f�rence'
); $VM_LANG->initModule( 'order', $langvars );
?>