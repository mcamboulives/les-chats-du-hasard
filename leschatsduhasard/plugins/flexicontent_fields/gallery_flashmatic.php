<?php
/**
 *
 * @version 1.7
 * @package Joomla
 * @subpackage FLEXIcontent
 * @subpackage plugin.gallery_flashmatic
 * @copyright (C) 2012 NetAssoPro - extensions.netassopro.com
 * @license GNU/GPL v2
 *
 * 09/01/12 - AJout de la compatibilit� Joomla 1.7 FLEXIcontent 2.0
 * 07/04/11 - Ajout de la suppression du fichier avant de le recr�er pour �viter un probl�me de droit
 * 19/10/11 - Correction d'un probl�me de notice � la ligne 238
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.event.plugin');

class plgFlexicontent_fieldsGallery_flashmatic extends JPlugin {
	var $_path17 = '';
	var $_dir17 = '';
	function plgFlexicontent_fieldsGallery_flashmatic(&$subject, $params)
	{
		parent::__construct($subject, $params);
		JPlugin::loadLanguage('plg_flexicontent_fields_gallery_flashmatic', JPATH_ADMINISTRATOR);
		if (version_compare(JVERSION, '1.6.0', '>=')) {
			$this->_dir17 = DS . 'gallery_flashmatic';
			$this->_path17 = '/gallery_flashmatic';
		}
	}

	function onDisplayField(&$field, $item)
	{
		// execute the code only if the field type match the plugin type
		if ($field->field_type != 'gallery_flashmatic') return;

		$document = &JFactory::getDocument();
		$flexiparams = &JComponentHelper::getParams('com_flexicontent');
		$mediapath = $flexiparams->get('media_path', 'components/com_flexicontent/media');
		$app = &JFactory::getApplication();
		$client = $app->isAdmin() ? '../' : '';
		$clientpref = $app->isAdmin() ? '' : 'administrator/';

		if (version_compare(JVERSION, '1.6.0', '>=')) {
			$inputNamePrefix = "custom[";
			$inputNameSuffix = "]";
		} else {
			$inputNamePrefix = "";
			$inputNameSuffix = "";
		}

		$js = "
		function randomString() {
			var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
			var string_length = 6;
			var randomstring = '';
			for (var i=0; i<string_length; i++) {
				var rnum = Math.floor(Math.random() * chars.length);
				randomstring += chars.substring(rnum,rnum+1);
			}
			return randomstring;
		}

		function qfSelectFile" . $field->id . "(id, file) {

			var name 	= 'a_name'+id;
			var ixid 	= randomString();
			var li 		= document.createElement('li');
			var thumb	= document.createElement('img');
			var hid		= document.createElement('input');
			var span	= document.createElement('span');
			var img		= document.createElement('img');

			var filelist = document.getElementById('sortables_" . $field->id . "');

			$(li).addClass('gallery_flashmatic');
			$(thumb).addClass('thumbs');
			$(span).addClass('drag" . $field->id . "');

			var button = document.createElement('input');
			button.type = 'button';
			button.name = 'removebutton_'+id;
			button.id = 'removebutton_'+id;
			$(button).addClass('fcbutton');
			$(button).addEvent('click', function() { deleteField" . $field->id . "(this) });
			button.value = '" . JText::_('FLEXI_REMOVE_FILE') . "';

			var pictitle = document.createElement('input');
			pictitle.type = 'text';
			pictitle.name = '" . $inputNamePrefix . $field->name . $inputNameSuffix . "['+ixid+'][title]';
			pictitle.id = 'pictitle'+id;
			var pictitle_label = document.createElement('label');
			pictitle_label.htmlFor = 'pictitle'+id;
			pictitle_label.innerHTML = '" . JText::_('FLEXI_FIELD_GALLERYFM_IMAGE_TITLE') . "';

			var picdesc = document.createElement('textarea');
			picdesc.name = '" . $inputNamePrefix . $field->name . $inputNameSuffix . "['+ixid+'][desc]';
			picdesc.id = 'picdesc'+id;
			picdesc.cols = '30';
			picdesc.rows = '3';
			var picdesc_label = document.createElement('label');
			picdesc_label.htmlFor = 'picdesc'+id;
			picdesc_label.innerHTML = '" . JText::_('FLEXI_FIELD_GALLERYFM_IMAGE_DESC') . "';

			var piclink = document.createElement('input');
			piclink.type = 'text';
			piclink.name = '" . $inputNamePrefix . $field->name . $inputNameSuffix . "['+ixid+'][link]';
			piclink.size = '30';
			piclink.id = 'piclink'+id;
			var piclink_label = document.createElement('label');
			piclink_label.htmlFor = 'piclink'+id;
			piclink_label.innerHTML = '" . JText::_('FLEXI_FIELD_GALLERYFM_IMAGE_LINK') . "';

			var pictarget = document.createElement('select');
			pictarget.name = '" . $inputNamePrefix . $field->name . $inputNameSuffix . "['+ixid+'][target]';
			pictarget.id = 'pictarget'+id;
			var pictarget_option1 = document.createElement('option');
			pictarget_option1.value = '_blank';
			pictarget_option1.text = '_blank';
			pictarget.add(pictarget_option1, null);
			var pictarget_option2 = document.createElement('option');
			pictarget_option2.value = '_self';
			pictarget_option2.text = '_self';
			pictarget.add(pictarget_option2, null);
			var pictarget_label = document.createElement('label');
			pictarget_label.htmlFor = 'pictarget'+id;
			pictarget_label.innerHTML = '" . JText::_('FLEXI_FIELD_GALLERYFM_IMAGE_LINK_TARGET') . "';

			thumb.src = '" . $client . "components/com_flexicontent/librairies/phpthumb/phpThumb.php?src=../../../../components/com_flexicontent/medias/'+file+'&w=60&h=60&zc=1';
			thumb.alt ='" . JText::_('FLEXI_CLICK_TO_DRAG') . "';

			hid.type = 'hidden';
			hid.name = '" . $inputNamePrefix . $field->name . $inputNameSuffix . "['+ixid+'][file]';
			hid.value = id;
			hid.id = ixid;

			img.src = '" . $clientpref . "components/com_flexicontent/assets/images/move3.png';
			img.alt ='" . JText::_('FLEXI_CLICK_TO_DRAG') . "';

			filelist.appendChild(li);
			li.appendChild(thumb);
			li.appendChild(pictitle_label);
			li.appendChild(pictitle);
			li.appendChild(picdesc_label);
			li.appendChild(picdesc);
			li.appendChild(piclink_label);
			li.appendChild(piclink);
			li.appendChild(pictarget_label);
			li.appendChild(pictarget);
			li.appendChild(button);
			li.appendChild(hid);
			li.appendChild(span);
			span.appendChild(img);

			new Sortables($('sortables_" . $field->id . "'), {
				'constrain': true,
				'opacity' :0.5,
				'handle': '.drag" . $field->id . "'
			});
		}
			function deleteField" . $field->id . "(el) {
				var field	= $(el);
				var row		= field.getParent();
				var fx = new Fx.Morph(row, {duration: 300, transition: Fx.Transitions.linear});

				fx.start({
					'height': 0,
					'opacity': 0
				}).chain(function(){
					row.destroy();
				});
			}
		";
		$document->addScriptDeclaration($js);
		// add the drag and drop sorting feature
		$js = "
			window.addEvent('domready', function(){
	    		new Sortables($('sortables_" . $field->id . "'), {
	    			'constrain': true,
	    			'opacity' :0.5,
	    			'handle': '.drag" . $field->id . "'
	    		});
	    	});
			";
		$document->addScriptDeclaration($js);
		if (version_compare(JVERSION, '1.6.0', '<')) {
			$document->addScript(JURI::root() . 'administrator/components/com_flexicontent/assets/js/sortables.js');
		}

		$css = '
			#sortables_' . $field->id . ' { margin: 0 0 10px 0; padding: 0px; list-style: none; white-space: nowrap; }
			#sortables_' . $field->id . ' li {
				list-style: none;
				height: 60px;
				padding-top:10px;
				}
			#sortables_' . $field->id . ' li img.thumbs {
				border: 1px solid silver;
				padding: 0;
				margin: 0 0 -5px 0;
				}
			#sortables_' . $field->id . ' li input { cursor: text;}
			#sortables_' . $field->id . ' li input.fcbutton, .fcbutton { cursor: pointer; margin-left: 3px; }
			span.drag img {
				margin: -4px 8px;
				cursor: move;
			}
			.button-add {	float:left; clear:both; margin-top:6px;	}
			.button-add a {
				display: block; float:left; padding:4px 8px;
				background-color: darkred; color:white; color:white!important; font-weight:bold;
				border-radius:4px; border:1px solid #eeeeee;
			}
			#sortables_' . $field->id . ' label,#sortables_' . $field->id . ' img, #sortables_' . $field->id . ' input , #sortables_' . $field->id . ' textarea,#sortables_' . $field->id . ' select {
			   vertical-align: middle;
			}

			';
		$document->addStyleDeclaration($css);

		$move = JHTML::image ('administrator/components/com_flexicontent/assets/images/move3.png', JText::_('FLEXI_CLICK_TO_DRAG'));

		JHTML::_('behavior.modal', 'a.modal_' . $field->id);

		$i = 0;
		$field->html = '<ul id="sortables_' . $field->id . '">';

		if ($field->value) {
			$values = unserialize($field->value[0]);
			foreach($values as $value) {
				$filename = $this->getFileName(@$value['file']);
				if (!is_null($filename)) {
					$field->html .= '<li class="gallery_flashmatic">';
					$src = $client . 'components/com_flexicontent/librairies/phpthumb/phpThumb.php?src=..' . DS . '..' . DS . '..' . DS . '..' . DS . $mediapath . DS . $filename->filename . '&w=60&h=60&zc=1';

					$field->html .= '<img class="thumbs" src="' . $src . '"/>';
					$field->html .= '<label for="pictitle' . $i . '">' . JText::_('FLEXI_FIELD_GALLERYFM_IMAGE_TITLE') . '</label><input type="text" id="pictitle' . $i . '" name="' . $inputNamePrefix . $field->name . $inputNameSuffix . '[' . $i . '][title]" value="' . $value['title'] . '" />';
					$field->html .= '<label for="picdesc' . $i . '">' . JText::_('FLEXI_FIELD_GALLERYFM_IMAGE_DESC') . '</label><textarea cols="30" rows="3" id="picdesc' . $i . '" name="' . $inputNamePrefix . $field->name . $inputNameSuffix . '[' . $i . '][desc]">' . $value['desc'] . '</textarea>';
					$field->html .= '<label for="piclink' . $i . '">' . JText::_('FLEXI_FIELD_GALLERYFM_IMAGE_LINK') . '</label><input size="30" type="text" id="piclink' . $i . '" name="' . $inputNamePrefix . $field->name . $inputNameSuffix . '[' . $i . '][link]" value="' . $value['link'] . '" />';
					if ($value['target'] == "_blank") {
						$blanktarget = "selected";
						$selftarget = '';
					} else {
						$selftarget = "selected";
						$blanktarget = '';
					}
					$field->html .= '<label for="pictargetlink' . $i . '">' . JText::_('FLEXI_FIELD_GALLERYFM_IMAGE_LINK_TARGET') . '</label><select id="pictargetlink' . $i . '" name="' . $inputNamePrefix . $field->name . $inputNameSuffix . '[' . $i . '][target]"><option ' . $selftarget . '>_self</option><option ' . $blanktarget . '>_blank</option></select>';
					$field->html .= '<input type="hidden" id="a_id' . $i . '" name="' . $inputNamePrefix . $field->name . $inputNameSuffix . '[' . $i . '][file]" value="' . $value['file'] . '" />';
					$field->html .= '<input class="inputbox fcbutton" type="button" onclick="deleteField' . $field->id . '(this);" value="' . JText::_('FLEXI_REMOVE_FILE') . '" />';
					$field->html .= '<span class="drag' . $field->id . '">' . $move . '</span>';
					$field->html .= '</li>';
					$i++;
				}
			}
		}
		$linkfsel = JURI::base() . 'index.php?option=com_flexicontent&amp;view=fileselement&amp;tmpl=component&amp;layout=image&amp;filter_secure=M&amp;index=' . $i . '&amp;field=' . $field->id . '&amp;' . JUtility::getToken() . '=1';
		$field->html .= "
						</ul>
						<div class=\"button-add\">
							<div class=\"blank\">
								<a class=\"modal_" . $field->id . "\" title=\"" . JText::_('FLEXI_ADD_FILE') . "\" href=\"" . $linkfsel . "\" rel=\"{handler: 'iframe', size: {x:window.getSize().x-100, y: window.getSize().y-100}}\">" . JText::_('FLEXI_ADD_FILE') . "</a>
							</div>
						</div>
						";
	}

	function onDisplayFieldValue(&$field, $item, $values = null, $prop = 'display')
	{
		$field->label = JText::_($field->label);
		// execute the code only if the field type match the plugin type
		if ($field->field_type != 'gallery_flashmatic') return;

		$values = $values ? $values : $field->value ;
		if ($values) {
			$values = @unserialize($values[0]);
			$document = &JFactory::getDocument();
			$flexiparams = &JComponentHelper::getParams('com_flexicontent');
			$mediapath = $flexiparams->get('media_path', 'components/com_flexicontent/media');

			$insertSWFOBJECT = $field->parameters->get('insertSWFOBJECT', 1);
			$width = $field->parameters->get('width', 600);
			$height = $field->parameters->get('height', 300);
			$random = $field->parameters->get('random', 2);
			$backgroundColor = $field->parameters->get('backgroundColor', 'FFFFFF');
			$backgroundTransparency = $field->parameters->get('backgroundTransparency', 100);
			$loaderColor = $field->parameters->get('loaderColor', '000000');
			$cellWidth = $field->parameters->get('cellWidth', 50);
			$cellHeight = $field->parameters->get('cellHeight', 50);
			$showMinTime = $field->parameters->get('showMinTime', 0.2);
			$showMaxTime = $field->parameters->get('showMaxTime', 1.5);
			$blur = $field->parameters->get('blur', 50);
			$netTime = $field->parameters->get('netTime', 0.5);
			$alphaNet = $field->parameters->get('alphaNet', 80);
			$netColor = $field->parameters->get('netColor', '000000');
			$controllerVisible = $field->parameters->get('controllerVisible', 1);
			$controllerBackgroundVisible = $field->parameters->get('controllerBackgroundVisible', 1);
			$prevNextVisible = $field->parameters->get('prevNextVisible', 1);
			$playBtVisible = $field->parameters->get('playBtVisible', 1);
			$autoPlay = $field->parameters->get('autoPlay', 1);
			$navigationButtonsColor = $field->parameters->get('navigationButtonsColor', '000000');
			$controllerDistanceX = $field->parameters->get('controllerDistanceX', 10);
			$controllerDistanceY = $field->parameters->get('controllerDistanceY', 10);
			$controllerHeight = $field->parameters->get('controllerHeight', 27);
			$distanceBetweenControllerElements = $field->parameters->get('distanceBetweenControllerElements', 10);
			$normalColor = $field->parameters->get('normalColor', '000000');
			$overColor = $field->parameters->get('overColor', '473C31');
			$selectedTextColor = $field->parameters->get('selectedTextColor', 'FFFFFF');
			$selectedButtonAlpha = $field->parameters->get('selectedButtonAlpha', 70);
			$distanceBetweenThumbs = $field->parameters->get('distanceBetweenThumbs', 7);
			$captionY = $field->parameters->get('captionY', 10);
			$captionX = $field->parameters->get('captionX', 10);
			$captionWidth = $field->parameters->get('captionWidth', 390);
			$buttonText = $field->parameters->get('buttonText', JText::_('FLEXI_FIELD_GALLERYFM_CAPTION_READMORE_TEXT_VALUE'));
			$btnNormalColor = $field->parameters->get('btnNormalColor', 'FFFFFF');
			$btnOverColor = $field->parameters->get('btnOverColor', '999999');
			$readMoreBackAlpha = $field->parameters->get('readMoreBackAlpha', 80);
			$readMoreBackColor = $field->parameters->get('readMoreBackColor', '473C31');
			$btnSpacingW = $field->parameters->get('btnSpacingW', 50);
			$btnSpacingH = $field->parameters->get('btnSpacingH', 5);
			$paddingX = $field->parameters->get('paddingX', 20);
			$paddingY = $field->parameters->get('paddingY', 15);
			$imageshow = $field->parameters->get('imageshow', 1);
			$bar_color = $field->parameters->get('bar_color', 'FFFFFF');
			$bar_transparency = $field->parameters->get('bar_transparency', 40);
			$slideshowTime = $field->parameters->get('slideshowTime', 4);
			// Variable Preconfiguration
			if ($random == '1') {
				$random = 'true';
			} else {
				$random = 'false';
			}
			if ($controllerVisible == '2') {
				$controllerVisible = 'false';
			} else {
				$controllerVisible = 'true';
			}
			if ($controllerBackgroundVisible == '2') {
				$controllerBackgroundVisible = 'false';
			} else {
				$controllerBackgroundVisible = 'true';
			}
			if ($prevNextVisible == '2') {
				$prevNextVisible = 'false';
			} else {
				$prevNextVisible = 'true';
			}
			if ($playBtVisible == '2') {
				$playBtVisible = 'false';
			} else {
				$playBtVisible = 'true';
			}
			if ($autoPlay == '2') {
				$autoPlay = 'false';
			} else {
				$autoPlay = 'true';
			}

			jimport('joomla.client.helper');
			JClientHelper::setCredentialsFromRequest('ftp');
			$ftp = JClientHelper::getCredentials('ftp');

			$file = dirname(__FILE__) . DS . 'gallery_flashmatic' . DS . 'xml' . DS . 'flashmatic_' . $field->id . '_' . $item->id . '.xml';
			$txt = '<?xml version="1.0" encoding="UTF-8"?><banner width="' . $width . '" height="' . $height
			 . '" startWith="1" random="' . $random . '" backgroundColor="0x' . $backgroundColor . '" backgroundTransparency="'
			 . $backgroundTransparency . '" cellWidth="' . $cellWidth . '" cellHeight="' . $cellHeight . '" showMinTime="'
			 . $showMinTime . '" showMaxTime="' . $showMaxTime . '" blur="' . $blur . '" netTime="' . $netTime . '" alphaNet="'
			 . $alphaNet . '" netColor="0x' . $netColor . '" overColor="0x' . $overColor . '" normalColor="0x' . $normalColor
			 . '" selectedTextColor="0x' . $selectedTextColor . '" selectedButtonAlpha="' . $selectedButtonAlpha
			 . '" controllerVisible="' . $controllerVisible . '" controllerBackgroundVisible="' . $controllerBackgroundVisible
			 . '" prevNextVisible="' . $prevNextVisible . '" playBtVisible="' . $playBtVisible . '" autoPlay="' . $autoPlay
			 . '" navigationButtonsColor="0x' . $navigationButtonsColor . '" controllerDistanceX="' . $controllerDistanceX
			 . '" controllerDistanceY="' . $controllerDistanceY . '" controllerHeight="' . $controllerHeight
			 . '" distanceBetweenControllerElements="' . $distanceBetweenControllerElements . '" distanceBetweenThumbs="'
			 . $distanceBetweenThumbs . '" captionY="' . $captionY . '" captionX="' . $captionX . '" captionWidth="'
			 . $captionWidth . '" buttonText="' . $buttonText . '" btnNormalColor="0x' . $btnNormalColor . '" btnOverColor="0x'
			 . $btnOverColor . '" readMoreBackAlpha="' . $readMoreBackAlpha . '" readMoreBackColor="0x' . $readMoreBackColor
			 . '" paddingX="' . $paddingX . '" paddingY="' . $paddingY . '" btnSpacingW="' . $btnSpacingW . '" btnSpacingH="'
			 . $btnSpacingH . '" loaderColor="0x' . $loaderColor . '">';

			foreach ($values as $value) {
				$filename = $this->getFileName(@$value['file']);
				if (!is_null($filename)) {
					$temp_path = trim($mediapath . '/' . $filename->filename);
					$temp_title = trim($value['title']);
					$temp_caption = trim($value['desc']);
					$temp_link = trim($value['link']);
					$temp_target = trim($value['target']);

					if (file_exists($temp_path)) {
						$temp_path = JURI::root() . $temp_path;
						if ($temp_title != '') {
							$temp_title = '<![CDATA[' . $temp_title . ']]>';
						} else {
							$temp_title = $temp_title;
						}
						if ($temp_caption != '') {
							$temp_caption = '<![CDATA[' . $temp_caption . ']]>';
						} else {
							$temp_caption = $temp_caption;
						}

						$temp_link = str_replace('&amp;', '&', $temp_link);
						$temp_link = str_replace('&', '&amp;', $temp_link);

						if (($temp_target == '_blank') || ($temp_target == '_self')) {
							$temp_target = $temp_target;
						} else {
							$temp_target = '_blank';
						}
						$txt .= '<item><path>' . $temp_path . '</path><title>' . $temp_title . '</title><caption>' . $temp_caption
						 . '</caption><target>' . $temp_target . '</target><link>' . $temp_link . '</link><bar_color>0x' . $bar_color
						 . '</bar_color><bar_transparency>' . $bar_transparency
						 . '</bar_transparency><caption_color>0xFFFFFF</caption_color><caption_transparency>60</caption_transparency><stroke_color>0xFFFFFF</stroke_color><stroke_transparency>60</stroke_transparency><slideshowTime>'
						 . $slideshowTime . '</slideshowTime></item>';
					}
				}
			}
			$txt .= '</banner>';
			jimport('joomla.filesystem.file');
			// Try to make the params file writeable
			if (JFile::exists($file))
				JFile::delete($file);
			JFile::write($file, $txt);
			JPath::setPermissions($file, '0755', '0755');

			if (!$ftp['enabled'] && !JPath::setPermissions($file, '0755')) {
				JError::raiseNotice('SOME_ERROR_CODE', JText::_('Could not make the file unwritable'));
			}
			// }
			if ($insertSWFOBJECT == '1') {
				$document->addScript(JURI::root() . 'plugins/flexicontent_fields/gallery_flashmatic' . $this->_path17 . '/js/swfobject.js');
			}

			$js_controller = 'var cacheBuster = "?t=" + Date.parse(new Date()); var stageW = "' . $width . '"; var stageH = "'
			 . $height . '"; var attributes = {}; attributes.id = \'Flashmatic'
			 . $field->id . '\'; attributes.name = attributes.id; var params = {}; params.bgcolor = "#'
			 . $backgroundColor . '"; params.menu = "false"; params.scale = "noScale"; params.wmode = "opaque"; params.allowfullscreen = "true"; params.allowScriptAccess = "always"; var flashvars = {}; flashvars.componentWidth = stageW; flashvars.componentHeight = stageH; flashvars.pathToFiles = ""; flashvars.xmlPath = "' . JURI::root() . 'plugins/flexicontent_fields' . $this->_path17 . '/gallery_flashmatic/xml/flashmatic_' . $field->id . '_' . $item->id . '.xml"; swfobject.embedSWF("' . JURI::root() . 'plugins/flexicontent_fields/gallery_flashmatic' . $this->_path17 . '/swf/preview.swf"+cacheBuster, attributes.id, stageW, stageH, "9.0.124", "' . JURI::root() . 'plugins/flexicontent_fields/gallery_flashmatic/swf/expressInstall.swf", flashvars, params, attributes);';
			$document->addScriptDeclaration($js_controller);

			$field->{$prop} = '<div id="Flashmatic' . $field->id
			 . '"><p>In order to view this object you need Flash Player 9+ support!</p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/></a><p>'
			 . base64_decode('UG93ZXJlZCBieQ==') . ' <a href="' . base64_decode('aHR0cDovL3d3dy5yc3dlYnNvbHMuY29t')
			 . '" target="_blank">' . base64_decode('UlMgV2ViIFNvbHV0aW9ucw==') . '</a></p></div>';
		}
	}

	function onBeforeSaveField($field, &$post, $file)
	{
		// execute the code only if the field type match the plugin type
		if ($field->field_type != 'gallery_flashmatic') return;
		// reformat the post
		$newpost = array();
		$new = 0;
		foreach ($post as $n => $v) {
			if ($post[$n]['file'] != '') {
				$newpost[$new]['file'] = $post[$n]['file'];
				$newpost[$new]['title'] = $post[$n]['title'];
				$newpost[$new]['desc'] = $post[$n]['desc'];
				if (!preg_match("#^http|^https|^ftp#i", $post[$n]['link']) && $post[$n]['link']) {
					$newpost[$new]['link'] = 'http://' . $post[$n]['link'];
				} else {
					$newpost[$new]['link'] = $post[$n]['link'];
				}
				$newpost[$new]['target'] = $post[$n]['target'];
				$new++;
			}
		}
		$post = $newpost;
		// create the fulltext search index
		$searchindex = '';

		foreach ($post as $v) {
			$searchindex .= $v['title'];
			$searchindex .= ' ';
			$searchindex .= $v['desc'];
			$searchindex .= ' ';
		}
		$searchindex .= ' | ';
		$field->search = $searchindex;
		if (count($post)) {
			$post = serialize($post);
		}
	}

	function getFileName($value)
	{
		$db = &JFactory::getDBO();
		$query = 'SELECT filename, altname, ext'
		 . ' FROM #__flexicontent_files'
		 . ' WHERE id = ' . (int) $value;
		$db->setQuery($query);
		$filename = $db->loadObject();
		return $filename;
	}
}