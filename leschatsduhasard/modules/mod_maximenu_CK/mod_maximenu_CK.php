<?php
/**
 * @copyright	Copyright (C) 2010 C�dric KEIFLIN alias ced1870
 * http://www.ck-web-creation-alsace.com
 * http://www.joomlack.fr
 * Module Maximenu_CK for Joomla! 1.5
 * @license		GNU/GPL
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

//check if we need Virtuemart for helper
$usevm = $params->get('usevm','0');
if ($usevm) {
	require_once (dirname(__FILE__).DS.'helper_vm.php');
        $items = modmaximenu_CKvmHelper::GetMenu($params);
} else {
	require_once (dirname(__FILE__).DS.'helper.php');
        $items = modmaximenu_CKHelper::GetMenu($params);
}



//check if we need Virtuemart for layout
if ($usevm) {
	require(JModuleHelper::getLayoutPath('mod_maximenu_CK','default_vm'));
} else {
	require(JModuleHelper::getLayoutPath('mod_maximenu_CK'));
}

?>