<?php // no direct access
defined('_JEXEC') or die('Restricted access');

require_once( $dirname . DS. 'reflect2.php' );
require_once( $dirname . DS. 'reflect3.php' );

	$dflttmpl = '<a class="highslide" href="{img}" onclick="return hs.expand(this)" title="{hreftitle}">'
		   .'<img src="{rflct}" alt="{imgalt}" title="{imgtitle}" name="{imgtitle}" /></a>';

	if ($params->get('outputtemplate') != '')
	{
		//
		//	load the template file
		//
		$dflttmpl = modImageFlowHelper::getTemplate( $params->get('outputtemplate') );
	}

	$dflttmpl .= PHP_EOL;

	if ($params->get('imageslider') == '1')
	{
		$scrpt = 'imf.hide_slider = false;'.PHP_EOL;
	}
	else
	{
		$scrpt = 'imf.hide_slider = true;'.PHP_EOL;
	}
	if ($params->get('captions') == '1')
	{
		$scrpt .= 'imf.hide_caption = false;'.PHP_EOL;
	}
	else
	{
		$scrpt .= 'imf.hide_caption = true;'.PHP_EOL;
	}


	if ($params->get('glidetoimage') != '')
	{
		$glidetoimage = $params->get('glidetoimage');

		if ($glidetoimage == 0) {
			//$half = (int) ((count($images)) * (1/2) + 0.9);

			$random = mt_rand(0,count($images));

			$catid =JRequest::getVar('id', 'none', 'get', 'int');

			if ($catid=='none' || !$params->get('glidetocurrent')) $glidetoimage = $random;

			else {
				$db		=& JFactory::getDBO();
				do {
					$query	= 'SELECT cc.ordering AS ordering, cc.id as id, cc.parent_id as parentid'
					. ' FROM #__phocagallery_categories AS cc, #__phocagallery AS ii'
					. ' WHERE cc.published = 1'
					. ' AND cc.id = ii.catid'
					. ' AND cc.id = '.$catid;
					$db->setQuery( $query );
					$res = $db->loadAssoc();

					$catid=$res['parentid'];
				}
				while ($res['parentid']);
				$glidetoimage=$res['ordering'];
			}
		}

		//	Have they given us a percentage?
		else if (JString::substr($glidetoimage, -1) == '%')
		{
			//	Yes, remove the % sign
			$glidetoimage = (int) JString::substr($glidetoimage, 0, -1);
			$glidetoimage = (int) (count($images)) * ($glidetoimage/100);
			$glidetoimage = (int) ($glidetoimage+.50);
		}
		else
		{
			$glidetoimage = (int) $glidetoimage;
		}
		if ($glidetoimage > count($images))
		{
			$glidetoimage = count($images);
		}
		if ($glidetoimage < 1)
		{
			$glidetoimage = 1;
		}
		$glidetoimage--;
		$scrpt .= 'imf.caption_id = '.$glidetoimage.';'.PHP_EOL;
		$scrpt .= 'imf.current = '.-$glidetoimage.' * imf.xstep;'.PHP_EOL;
	}
	if ($params->get('imagethumbnailclass') != '')
	{
		$scrpt .= "imf.conf_thumbnail = '".$params->get('imagethumbnailclass')."';".PHP_EOL;
	}
	if ($params->get('reflectheight') != '')
	{
		$height = JString::str_ireplace('%', '', $params->get('reflectheight'));
		$height = ($height / 100);
		$scrpt .= 'imf.conf_reflection_p = '.$height.';'.PHP_EOL;
	}

	$path_relative = JString::str_ireplace(JPATH_BASE, '', $dirname );
	$path_relative = JPath::clean( $path_relative, '/');
	$modpath = JURI::root(true) . $path_relative . '/';
	$document = &JFactory::getDocument();
	$document->addScriptDeclaration( $scrpt );
	$document->addScript( $modpath.'imageflow.js');
	if ($params->get('cssfile') != '')
	{
		$cssfile = $params->get('cssfile');
		$cssfile = JURI::root(true).'/'.$cssfile;
		$document->addStyleSheet( $cssfile );
	}
?>
<div id="imageflow" class="imageflow<?php echo $params->get('moduleclass_sfx'); ?>">
	<div id="imageflow_loading">
		<b>Loading images</b><br/>
		<img src="<?php echo $modpath.'loading.gif'; ?>" width="208" height="13" alt="loading" />
	</div>
	<div id="imageflow_images">
<?php
	for ($i=0; $i<count($images); $i++)
	{
		$imagename = $images[$i]['img'];
		$rimagename = JPATH_BASE . DS . $images[$i]['img'];
		$rimagename = JPath::clean( $rimagename, DS );
		if (JFILE::exists($rimagename))
		{
			$imagename = JPath::clean( JURI::root(true). '/'. $imagename, '/' );

			$height = $params->get('reflectheight');
			if($height == '')
			{
				$height = '50%';
			}
			if (JString::substr($height,-1) != '%')
			{
				$height .= '%';
			}
			if ($height == "0%")
			{
				$rflctimage	= $imagename;
			}
			else
			{
				$rflctparams['height'] = $height;
				$rflctparams['img'] = $rimagename;
				$bgc = $params->get('fadetocolor');
				$bgc = JString::str_ireplace('#', '', $bgc);
				if ($bgc != '')
				{
					$rflctparams['bgc'] = $bgc;
				}
				$tint = $params->get('tintcolor');
				$tint = JString::str_ireplace('#', '', $tint);
				if ($tint != '')
				{
					$rflctparams['tint'] = $tint;
				}
				if ($params->get('reflectresize') != '')
				{
					$rflctparams['resize'] = $params->get('reflectresize');
				}
				if ($params->get('alphareflect') == '0')
				{
					$rflctimage = Reflect2::create( $rflctparams );
				}
				else
				{
					$rflctimage = Reflect3::create( $rflctparams );
				}
				if ($rflctimage !== false)
				{
					if (JString::substr($rflctimage, 0, JString::strlen(JPATH_BASE)) == JPATH_BASE)
					{
						$rflctimage = JString::substr( $rflctimage, JString::strlen(JPATH_BASE), JString::strlen($rflctimage)-JString::strlen(JPATH_BASE) );
					}
					$rflctimage = JPath::clean( JURI::root(true). '/'. $rflctimage, '/' );
				}
			}
			if ($rflctimage !== false)
			{
				$tmpl = $dflttmpl;
				if (isset( $images[$i]['tmplt'] ))
				{
					$tmpltoverride = modImageFlowHelper::getTemplate($images[$i]['tmplt'] );
					if ($tmpltoverride != "")
					{
						$tmpl = $tmpltoverride.PHP_EOL;
					}
				}
				$def = JString::str_ireplace('{img}', $imagename, $tmpl );
				$def = JString::str_ireplace('{rflct}', $rflctimage, $def  );
				foreach( $images[$i] as $kwd => $val)
				{
					$def = JString::str_ireplace('{'.$kwd.'}', $val, $def);
				}
				echo $def;
			}
		}
	}
?>
	</div>
	<div id="imageflow_captions"></div>
	<div id="imageflow_scrollbar">
		<div id="imageflow_slider"></div>
	</div>
</div>